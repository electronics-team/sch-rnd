<html>
<body>

<H2 autotoc="yes"> target_spice: automatic bridge </H2>
<p>
In some cases invisible bridges need to be inserted at ports. A typical
example is ngspice mixed mode simulation (releases before 2023)
where ADC or DAC components have to be applied in between the analog and
the digital side of the circuit. It would be possible to add these
converters manually on the schematics, but that'd clutter the drawing.
An alternative approach is using automatic bridges specified in
terminal attributes.
<p>
The relevant terminal attributes are spice/bridge/model and spice/bridge/dir.
When <b>spice/bridge/model</b> is present, it names a spice model (in
the sheet local spice lib or in the external spice lib) that is placed
at the port by target_spice. This placement happens in the abstract mode:
first the original network connection is cut at the port, then a new <i>instance</i>
of the converter is inserted, connected to the original network
and a new network segment is introduced to connect the other port of the
converter and the original port.
<p>
What <i>instance</i> really means depends on the config node
plugins/target_spice/bridges, which should be one of the following
string values:
<ul>
	<li> <b>vector</b>: for ngspice it is enough to create one component per
	     converter type and it acts as a multi-slot or multi-port converter
	     array with vector input and vector output.
	<li> <b>scalar</b>: for xyce a discrete converter needs to be placed per
	     port.
	<li> <b>none</b>: ignore the spice/bridge attributes and do not create
	     any converter or bridge; this assumes the models are all analog or
	     the spice implementation handles conversion automatically.
</ul>
<p>
The converter is assumed to have only 2 ports, one input and one output.
Order of the connections, or in other words direction of the converter
matters. This can be controlled by the optional <b>spice/bridge/dir</b>
terminal attribute, which should be either "n1p2" or "p1n2". The former
means "original network on port 1 of the converter, original port on
port 2 of the converter", the latter is the opposite. When spice/bridge/dir
is not specified, the code makes an educated guess by the model name,
which should contain either "adc" or "dac", assuming the attribute
is placed on the terminal of a digital port connected to an analog net.
<p>
For example, on the drawing below U1 is a digital device that has two ports,
one input, one output. The input port has spice/bridge/model set to
<i>bridge_adc_ttl</i>, the outpout port has it set to <i>bridge_dac_ttl</i>;
spice/bridge/dir is not set. These models are shipped with sch-rnd stock spice
lib.
<p>
<img src="bridge.svg" width=600>
<p>
Since directions are not set, they are assumed n1p2 for *adc* on the input
and p1n2 for the *dac* on the output. This means the adc's port1 is
connected to the original network (orig_analog_1), port2 to the original
(digital) port of U1, while on the dac port1 is connected to U1's port and
port 2 is to the original network (orig_analog_2). Two dummy networks are
introduced. These are all shown in the abstract model and end up on the
exported netlist, but they are not graphically visible on the drawing.
