ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=Id/fKeXcFfUWvnGHBKcAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.9 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAF;
				x=40000; y=60000;
				li:objects {
					ha:group.1 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAG; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAD;
						x=28000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=output
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAH; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=input
							role=terminal
						}
					}
					ha:line.3 { x1=4000; y1=8000; x2=4000; y2=-8000; stroke=sym-decor; }
					ha:line.4 { x1=4000; y1=-8000; x2=24000; y2=-8000; stroke=sym-decor; }
					ha:line.5 { x1=24000; y1=-8000; x2=24000; y2=8000; stroke=sym-decor; }
					ha:line.6 { x1=24000; y1=8000; x2=4000; y2=8000; stroke=sym-decor; }
					ha:text.7 { x1=8000; y1=0; dyntext=0; stroke=sym-decor; text=digital device; }
					ha:text.9 { x1=4000; y1=-11000; dyntext=1; stroke=sym-secondary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=U1
					role=symbol
				}
			}
			ha:group.23 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAO;
				x=-28000; y=-84000;
				li:objects {
					ha:line.1 { x1=68000; y1=144000; x2=44000; y2=144000; stroke=wire; }
					ha:text.2 { x1=44000; y1=144000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=orig_analog_1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.25 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAP;
				x=-28000; y=-84000;
				li:objects {
					ha:line.1 { x1=96000; y1=144000; x2=124000; y2=144000; stroke=wire; }
					ha:text.2 { x1=112000; y1=144000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=orig_analog_2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.27 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAQ;
				x=4000; y=4000;
				li:objects {
					ha:group.1 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAR; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAI;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAS; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAJ;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=5000; y1=-1500; dyntext=0; stroke=sym-decor; text=ADC; }
					ha:line.4 { x1=4000; y1=4000; x2=4000; y2=-4000; stroke=sym-decor; }
					ha:line.5 { x1=4000; y1=-4000; x2=16000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=16000; y1=0; x2=4000; y2=4000; stroke=sym-decor; }
				}
				ha:attrib {
					name=REFDES
					role=symbol
				}
			}
			ha:group.28 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAT;
				x=84000; y=4000;
				li:objects {
					ha:group.1 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAU; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAI;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAV; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAJ;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=5000; y1=-1500; dyntext=0; stroke=sym-decor; text=DAC; }
					ha:line.4 { x1=4000; y1=4000; x2=4000; y2=-4000; stroke=sym-decor; }
					ha:line.5 { x1=4000; y1=-4000; x2=16000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=16000; y1=0; x2=4000; y2=4000; stroke=sym-decor; }
				}
				ha:attrib {
					name=REFDES
					role=symbol
				}
			}
			ha:line.29 { x1=52000; y1=48000; x2=52000; y2=32000; stroke=sheet-decor; }
			ha:line.30 { x1=52000; y1=32000; x2=48000; y2=32000; stroke=sheet-decor; }
			ha:line.31 { x1=48000; y1=32000; x2=54000; y2=20000; stroke=sheet-decor; }
			ha:line.32 { x1=54000; y1=20000; x2=60000; y2=32000; stroke=sheet-decor; }
			ha:line.33 { x1=60000; y1=32000; x2=56000; y2=32000; stroke=sheet-decor; }
			ha:line.34 { x1=56000; y1=32000; x2=56000; y2=48000; stroke=sheet-decor; }
			ha:line.35 { x1=56000; y1=48000; x2=52000; y2=48000; stroke=sheet-decor; }
			ha:group.36 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAb; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAO;
				x=-64000; y=-140000;
				li:objects {
					ha:line.1 { x1=68000; y1=144000; x2=56000; y2=144000; stroke=wire; }
					ha:text.2 { x1=56000; y1=144000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=orig_analog_1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.37 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAc; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAP;
				x=7000; y=-140000;
				li:objects {
					ha:line.1 { x1=97000; y1=144000; x2=112000; y2=144000; stroke=wire; }
					ha:text.2 { x1=100000; y1=144000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=orig_analog_2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.38 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAd; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAF;
				x=40000; y=4000;
				li:objects {
					ha:group.1 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAe; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAD;
						x=28000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=output
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Id/fKeXcFfUWvnGHBKcAAAAf; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=input
							role=terminal
						}
					}
					ha:line.3 { x1=4000; y1=8000; x2=4000; y2=-8000; stroke=sym-decor; }
					ha:line.4 { x1=4000; y1=-8000; x2=24000; y2=-8000; stroke=sym-decor; }
					ha:line.5 { x1=24000; y1=-8000; x2=24000; y2=8000; stroke=sym-decor; }
					ha:line.6 { x1=24000; y1=8000; x2=4000; y2=8000; stroke=sym-decor; }
					ha:text.7 { x1=8000; y1=0; dyntext=0; stroke=sym-decor; text=digital device; }
					ha:text.9 { x1=4000; y1=8000; dyntext=1; stroke=sym-secondary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=U1
					role=symbol
				}
			}
			ha:group.42 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAg;
				x=-28000; y=-84000;
				li:objects {
					ha:line.1 { x1=52000; y1=88000; x2=68000; y2=88000; stroke=wire; }
					ha:text.2 { x1=56000; y1=88000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=dummy1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.46 {
				uuid=Id/fKeXcFfUWvnGHBKcAAAAk; src_uuid=Id/fKeXcFfUWvnGHBKcAAAAg;
				x=16000; y=-84000;
				li:objects {
					ha:line.1 { x1=52000; y1=88000; x2=68000; y2=88000; stroke=wire; }
					ha:text.2 { x1=59000; y1=88000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=dummy2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.59 {
				li:conn {
					/2/23/1
					/2/9/2/1
				}
			}
			ha:connection.60 {
				li:conn {
					/2/25/1
					/2/9/1/1
				}
			}
			ha:connection.61 {
				li:conn {
					/2/36/1
					/2/27/2/1
				}
			}
			ha:connection.62 {
				li:conn {
					/2/37/1
					/2/28/1/1
				}
			}
			ha:connection.63 {
				li:conn {
					/2/42/1
					/2/27/1/1
				}
			}
			ha:connection.64 {
				li:conn {
					/2/42/1
					/2/38/2/1
				}
			}
			ha:connection.65 {
				li:conn {
					/2/46/1
					/2/28/2/1
				}
			}
			ha:connection.66 {
				li:conn {
					/2/46/1
					/2/38/1/1
				}
			}
		}
		ha:attrib {
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 0
     grid = 1.0240 mm
    }
   }
  }
}
