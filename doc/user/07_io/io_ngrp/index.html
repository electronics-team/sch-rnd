<html>
<body>


<H2 autotoc="yes"> Non-graphical sheets </h2>
<p>
Sometimes a design is easier specified by text than drawing. Typical examples:
<ul>
	<li> "wiring diagram": there are two large-pin-number components, e.g. two
	     connectors and a bunch of connections between them (not necessary
	     1-to-1 wiring)
	<li> "breakout boards": large pin number central component
	     and mostly 1-to-1 wiring to other, smaller pin number components
	     (e.g. connectors).
	<li> "fpga", "ram": multiple large pin number components, with a lot
	     of 1-to-1 connections (that would often have the same name as the
	     terminal on both ends, by the way); for example when connecting
	     ram to a CPU: D0..D16, A0..A20, etc.
</ul>
<p>
Non-graphical sheets are implemented in plugins and provide different
methods and syntax for specifying abstract components, abstract networks
and their relations. The cschem compiler can use this as an input on the same
level as a graphical sheet. That means, a non-graphical sheet can be an Nth sheet in
a multisheet project. For example draw the power and analog parts on graphical
sheets and have the 28 pin parallel bus connections to the CPU, ram, I/O chips
and connectors specified in one more more non-graphical sheet in the same
project.
<p>
Below are different methods and syntax to describe non-graphical sheets.

<h3> tEDAx (declarative) </h3>
<p>
tEDAx is very easy to parse, well specified, and is relatively easy to
read and write as human. Implemented by io_ngrp_tedax.
<p>
Here are a few small examples, all describing the same "7805 with 2 bypass
caps" circuit using different approaches:
<ul>
	<li> <a href="../../../examples/non-graphical/small1.tdx"> small1.tdx</a>: component-centric 
	<li> <a href="../../../examples/non-graphical/small2.tdx"> small2.tdx</a>: network-centric 
	<li> <a href="../../../examples/non-graphical/small21.tdx"> small21.tdx</a>: mixture of component-centric and network-centric
	<li> <a href="../../../examples/non-graphical/small3.tdx"> small3.tdx</a>: compact, netlist-like
</ul>
<p>
These are not different sets of syntax, but they are all just different usage
and combination of the same few blocks that are all available. As a proper
declarative description, order of blocks or order of attributes/connections
within a block doesn't matter.
<p>
The verbose versions are perhaps more practical if there are a lot of attributes
to specify. The compact version is more practical for listing a lot of
connections and few attributes.
<p>
Another way of mixing different blocks is <a href="big32.tdx">big32.tdx</a>,
which is an imaginary CPU board with 2 SRAM chips, an I/O chip and an extension
port. It uses the verbose component description for the chips and capacitors,
also connects power and gnd there and then uses the compact, table-like
description for the address/data/control networks. It's also an example
of combining graphical and non-graphical sheets: U5 is a network of gates
that controls chip-select; it's better drawn for clarity.
<p>
Summary of blocks:
<table border=1 cellspacing=0>
	<tr> <th> block                  <th> description
	<td> cschem_acomp   <i>name</i>  <td> create a new (abstract) component; <i>name</i> is the name of the component (e.g. "R17"); can add component attributes and create connections from ports of the component to nets
	<td> cschem_anet    <i>name</i>  <td> create a new (abstract) network; <i>name</i> is the name of the net (e.g. "Vcc"); can add net attributes and create connections from the net to ports of components
	<td> cschem_acompact             <td> compact syntax that can describe both components and nets, multiple of them
</table>
<p>

<p>
Content of a cschem_acomp block: each line is one of these:
<ul>
	<li> if first field is a dash, the second field is a port number of the current
	     component and the third field is the name of a net; e.g. "- 4 Vcc" will
	     connect port 4 to the Vcc net
	<li> else the first field is an attribute key and the rest of the fields is an
	     attribute value; e.g. "footprint 1206" adds a <i>footprint=1206</i> attribute
	     to the current component; or "comment hand solder" adds a
	     <i>comment=hand solder</i>
</ul>
<p>
Content of a cschem_anet block: each line is one of these:
<ul>
	<li> if first field is a dash, the second field is a component name and
	     the third field is a port name; e.g. "- U12 4" will
	     connect the current net to port 4 of component U12
	<li> else the first field is an attribute key and the rest of the fields is an
	     attribute value; e.g. "current 250mA" adds a <i>current=250mA</i> attribute
	     to the current net; or "comment do not cover with mask" adds a
	     <i>comment=do not cover with mask</i>
</ul>
<p>
Content of a cschem_acompact block: each line is one of these:
<p>
<table border=1 cellspacing=0>
	<tr> <th> line                    <th> description
	<td> comp <i>cname</i> [attrs...] <td> create a new (abstract) component; <i>cname</i> is the name of the component (e.g. "R17"); followed by zero or more attributes (see below)
	<td> net <i>nname</i> [attrs...]  <td> create a new (abstract) network; <i>nname</i> is the name of the net (e.g. "Vcc"); followed by zero or more attributes (see below)
	<td> conn <i>nname</i> ports...   <td> creates one or more connections from the network called <i>nname</i> to ports (see below)
</table>
<p>
Attrs are specified in key=value syntax, e.g. footprint=1206.
If key or value contains whitespace, it needs to be escaped,
as per tEDAx syntax, using backslash.
<p>
Ports are specified in a comp=port syntax, e.g. U12=4 for port 4 of component U12.
If component or port name contains whitespace, it needs to be escaped,
as per tEDAx syntax, using backslash.


<h3> fawk (Turing-complete) </h3>
<p>
In some cases parts of the design can be specified using repetitive patterns.
An easy way to handle this is to write a script that generates a graphical
sheet or a non-graphical sheet using a declarative syntax.
<p>
If that script is written in fawk, sch-rnd can run it without having to
generate and parse the declarative intermediate. Implemented by io_ngrp_fawk.
This feature is limited to fawk because:
<ul>
	<li> fawk is always available in librnd
	<li> fawk supports execution limit and memory allocation limit so opening
	     a non-graphical sheet specified as a fawk script is safe, an attacker
	     won't be able to force sch-rnd into an infinite loop or into allocating
	     all system memory.
</ul>
<p>
A simple example script, <a href="../../../examples/non-graphical/r2r.fawk">r2r.fawk</a> generates
5 bits of the classic R2R DAC. It can be configured by changing the values
in main().
<p>
The script doesn't emit a netlist or declarative tEDAx non-graphical sheet.
The result can be observed in sch-rnd by loading only the fawk script
as a sheet and using the abstract model dialog.
<p>
(An alternative is implementing the cschem functions locally, as vararg
fawk functions and make them print to stdout using fawk_print(). Then the
script can be ran and debugged independently of cschem, using the example
libfawk command line interpreter.)
<p>
Functions provided by sch-rnd:
<table border=1 cellspacing=0>
	<tr> <th> function <th> description
	<tr> <td>acomp_attr(name,   key, val, ...);
	     <td>create a component by name, or add to existing component by that name;
	         add key=val pairs of attributes; this is a vararg function,
	         accepting zero or more key,val pairs.

	<tr> <td>anet_attr(name,   key, val, ...);
	     <td>create a network by name, or add to existing network by that name;
	         add key=val pairs of attributes; this is a vararg function,
	         accepting zero or more key,val pairs.

	<tr> <td>aconn(netname,   compname, termname, ...);
	     <td>create a network by netname, or extend existing network by that name;
	         connect terminal(s) addressed by (compname-termname); this is a
	         vararg function, accepting one or more compname,termname pairs.

</table>




</body>
</html>
