<html>
<body>
<h1> 7. cschem implementation - device mapping </h1>

<h2> 7.1. The transistor problem; heavy vs. light symbols </h2>

<h3> 7.1.1 PCB example </h3>

<p>{imp7:0}
The transistor problem is that a transistor symbol containing pin
numbers will not always match the PCB footprint's pin numbering. This
is especially annoying when the same symbol is to be used with signal
(sot23) and power (to220) transistors.

<p>{imp7:1}
Classic solution 'A' is simply having different, heavy symbols for each
footprint. This approach has the obvious problem that replacing the
footprint means replacing the symbol as well and that symbol needs
to be copied for each footprint variant.

<p>{imp7:2}
A less obvious problem is that the symbol-&gt;pcb-footprint mapping is
only one aspect, for one workflow (f1). If there's another flow (f2) that
may also have different mappings for the same symbol, it results in multiple
combinations of f1+f2 mappings. This easily leads to an explosion of heavy
symbols even for the very same device.

<p>{imp7:3}
Classic solution 'B' is to craft the footprint to match the symbol. This
means there's no sot23 footprint, but a sot23-2n7002 footprint. In other
words, the same sot23 footprint is copied over and over with only pin numbering
changes, forming "heavy footprints". Just like with heavy symbols, this easily
gets out of hand as well: what if different board densities or soldering methods
are to be supported? Instead of having 3 or 4 different sot23 footprints for
the different requirements, this method requires (3 or 4 processes) * (number
of different devices) clones of the sot23 footprints.

<p>{imp7:4}
While both of the above solutions would work with cschem, it is recommended
to use the devmap mechanism instead. For the 2n7002-in-sot23 example, the devmap
would mean:
<ul>
	<li> the schematics symbol is a generic MOSFET symbol; it does not have
	     pinseq, pinnumber or anything alike, only a <i>name</i> attribute on
	     each port, set to G, D or S
	<li> the symbol has no footprint attribute either
	<li> the symbol has a <i>devmap=2n7002_sot23</i> attribute
	<li> there is a file called 2n7002_sot23 in the devmap database or library
	<li> the devmap file specifies that pcb/footprint attribute should be set to
	     sot23 and the portmap attribute should be set to:
<pre>
G->pcb/pinnum=1
S->pcb/pinnum=2
D->pcb/pinnum=3
</pre>
	<li> the sot23 footprint is generic and has numbered pins, 1, 2 and 3
</ul>

<h3> 7.1.2 SPICE example </h2>

<p>{imp7:5}
The same example for SPICE simulation works the same way, except the devmap
file would set <i>spice/model=M</i> and a different <i>portmap</i> array:
<pre>
G->spice/pinnum=2
S->spice/pinnum=3
D->spice/pinnum=1
</pre>

<h2> 7.3. Combining the two </h2>

<p>{imp7:6}
A schematic can be the source of multiple workflows, e.g. both simulation
and PCB layout. An easy way to specify the device mapping for multiple flows is to
include them all in the same devmap file. The resulting file would set all
pcb/ and spice/ attributes and the portmap array would look like this:
<pre>
G->pcb/pinnum=1
G->spice/pinnum=2
S->pcb/pinnum=2
S->spice/pinnum=3
D->pcb/pinnum=3
D->spice/pinnum=1
</pre>

<p>{imp7:7}
However, this again creates bad coupling: for PCB, the devmap file name
includes the footprint, because that makes the actual binding between the
generic symbol and generic footprint. This mean there would be a different
devmap file for the same MOSFET part coming in a to92 package. Yet, the spice
model attribute and port mapping would be copied into both.

<p>{imp7:8}
A better way is to have a pcb-only devmap file called "2n7002_sot23" and a
SPICE specific devmap file called "2n7002_spice". The devmap attribute of
the generic MOSFET symbol placed for the device can be an array, listing both
devmap files. This keeps them decoupled, a footprint change does not affect
the simulation mapping.

<h2> 7.2. Slotting </h2>

<p>{imp7:9}
Slotting works by the <i>slot</i> attribute. Each entry of the <i>portmap</i>
attribute may contain a slot reference on the left side. When present, it
limits the use of the entry to symbols with matching slot name. This allows
assignment of the same terminal names multiple times, to different physical
pin numbers, depending on the slot attribute.

<p>{imp7:10}
For example the PCB devmap for common <a href="07_devmap.html#des7:7"> dual
opamp</a> in a 8 pin package, using slots 'A' and 'B':
<pre>
A/out -&gt; pcb/pinnum=1
A/in_minus -&gt; pcb/pinnum=2
A/in_plus -&gt; pcb/pinnum=3
B/out -&gt; pcb/pinnum=7
B/in_minus -&gt; pcb/pinnum=6
B/in_plus -&gt; pcb/pinnum=5
v_plus -&gt; pcb/pinnum=8
v_minus -&gt; pcb/pinnum=4
</pre>

<p>{imp7:11}
When both slots are present, using this portmap will produce the following
port names in the resulting abstract model component: A/out, A/in_minus,
A/in_plus, B/out, B/in_minus, B/in_plus, v_plus, v_minus. If slot A is
missing from the schematics, only B/out, B/in_minus, B/in_plus, v_plus,
v_minus are added in the abstract model.

<p>{imp7:12}
Note how v_plus and v_minus are specified to match universally, regardless
of the slot. This allows a symbol where both instances have v_plus and v_minus
terminals, but also allows a separate "opamp power" symbol with an arbitrary
slot name (e.g. slot=power) that contains only the power terminals. They are
not prefixed with A/ or B/ because they had the noslot attribute (in the
terminal).

<p>{imp7:13}
The resulting devmap file could be called "lm358_dip8", "lm358_so8" (and
can set attribute <i>device=lm358</i> on the resulting component). But
this pinout is a de-facto standard for dual ompamps in 8 pin packages, so
it could also be called "opamp2_dip8", "opamp2_so8" or even just
"opamp2_any8" - but in this case the <i>device=lm358</i> can not be part
of the devmap file.

<p>{imp7:14}
Note that this leads to the same heavy vs. light symbol consideration,
applied to devmap files now. Cschem supports both approaches, it is up
to the user to decide whether heavy or light devmap files should be used
in the user's own library of devmap files.

<h2> 7.3. How does this differ from gschem for the user? </h2>

<h3> 7.3.1. Device map </h3>

<p>{imp7:15}
Option A: do the same as in gschem: manually set "pin numbers" and all
attributes specific to footprints and simulation. This obviously reproduces
the transistor problem, easily resulting in mismatched footprints. It also
reproduces the heavy vs. light symbol problem.

<p>{imp7:16}
Option B: instead of filling in the footprint attribute, fill in the
devmap attribute. Instead of saying it is a "footprint=sot23" (and maybe
adding a device=2n7002 or value=2n7002), say it is a "devmap=2n7002_sot23".
Instead of filling in the physical pinout in a heavy symbol, write it into
a devmap file.


<h3> 7.3.2. Slotting </h3>

<p>{imp7:17}
Instead of the slotdef attribute, use the devmap attribute that is
pointing to a slotted devmap file. Using the slot attribute is the same,
except the value of the slot attribute doesn't have to be an integer
but can be text, e.g. "power", "gpio", "i2c".

<p>{imp7:18}
Instead of the somewhat cryptic slotdef attribute, the devmap file contains
a more readable table of pin assignment.

<h2> 7.4. How it is displayed on the UI? </h2>

<p>{imp7:19}
After devmap or portmap runs, the resulting attributes are available in the
abstract model. Cschem can trace the connection between the concrete
model's schematics symbol/terminal and the abstract model's component/port.

<p>{imp7:20}
Using this connection, a plugin in turn can take the resulting pcb/pinnum or
spice/pinnum attribute from the abstract model and display it at any terminal
on the schematics that contributed to it. Whether pcb/pinnum or spice/pinnum is
displayed that way, depends on the plugin (or plugin parameters), which is
configured in the view.

<p>{imp7:21}
In short: a view can be set up to display PCB pin numbers and another view
can be set up to display SPICE pin numbers and the user can switch between
views any time.
