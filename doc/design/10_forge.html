<html>
<body>
<h1> 10. cschem - forge </h1>

<p>{des10:0}
The purpose of the forge plugin is to provide an easy to implement yet generic
way for describing attribute transformations in attributes (e.g. in a
library symbol) during compilation.

<h2> 10.1. Unconditinal forge </h1>

<p>{des10:1}
The forge plugin works from the array attribute <i>forge</i>, which is
taken as an ordered list of atomic steps to execute on attributes. For
portability of data, the forge plugin needs to implement exactly the
operations described below.

<p>{des10:2}
Each entry of the array is one operation. An operation is split into tokens
separated by the separator character, which is a non-alphanumeric character
following the operator (first token). For example <i>sub,^dip,DIP,footprint</i> is a valid
operation that uses regexp to replace leading "dip" to DIP in the footprint
attribute. Since sub is followed by a comma, the token separator is comma.

<p>{des10:3}
Operations work with the current values of attributes at the time
of plugin execution during compilation of the abstract model. If destination
attribute doesn't exist, it is created.

<p>{des10:4}
If referenced source attribute of an operation does not exist, the operation
is silently skipped.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des10:5} Operation summary
<tr><th> operations and arguments <th> in-place<th> array?  <th> short description
<tr><td> sub,pat,str,attr         <td> yes     <td> map     <td> substitutes one match of regex pattern <i>pat</i> with <i>str</i> in <i>attr</i>
<tr><td> gsub,pat,str,attr        <td> yes     <td> map     <td> substitutes all matches of regex pattern <i>pat</i> with <i>str</i> in <i>attr</i>
<tr><td> suba,pat,ref,attr        <td> yes     <td> map     <td> substitutes one match of regex pattern <i>pat</i> with the value of the <i>ref</i> attribute in <i>attr</i>
<tr><td> gsuba,pat,ref,attr       <td> yes     <td> map     <td> substitutes all matches of regex pattern <i>pat</i> with the value of the <i>ref</i> attribute in <i>attr</i>
<tr><td> delete,attr              <td> yes     <td> atomic  <td> deletes (removes) the attribute
<tr><td> scalar,attr              <td> yes*    <td> atomic  <td> create <i>attr</i> as scalar if it does not exist; throw an error if it exists as an array
<tr><td> array,attr               <td> yes*    <td> atomic  <td> create <i>attr</i> as array if it does not exist; throw an error if it exists as a scalar
<tr><td> copy,dstattr,srcattr     <td> no      <td> atomic  <td> copies the value of <i>srcattr</i> to the destination attribute <i>dstattr</i> (overwriting or creating it)
<tr><td> append,dstattr,srcattr   <td> no      <td> darr    <td> appends the value of the source attribute <i>srcattr</i> to the value of the destination attribute <i>dstattr</i>
<tr><td> prepend,dstattr,srcattr  <td> no      <td> darr    <td> prepends the value of the source attribute <i>srcattr</i> to the value of the destination attribute <i>dstattr</i>
</table>

<p>{des10:6}
In-place operations modify existing attributes in place. They are no-op for
non-existing attributes.

<p>{des10:7}
If addressed attribute of the operation is an array, the operation will
behave according to the "array?" column:
<ul>
	<li> <b>map</b>: on copy, an array is mapped into an array; on edit-in-place each array element is edited individually
	<li> <b>atomic</b>: if array, the whole array is dealt with at once, as an atomic object; if scalar, result is a scalar too
	<li> <b>darr</b>: if destination is an array, the source (or each element of the source, if it's an array) is combined into the destination array. Fails if source is array and destination is scalar.
</ul>

<h2 id="des10.1"> 10.1. Conditinal forge </h2>
<p>{des10:8}
Attributes whose key start with <b>forge-if/</b> are used for conditional
execution of forge. The remainder of the attribute key is an arbitrary
identifier, for example forge-if/dnp is commonly used for setting a DNP
(do-not-populate) attribute depending on the current view and/or stances.

<p>{des10:9} The attribute is an array; the first entry contains
the condition; the rest of the entries contain the same forge operations
as the unconditional <i>forge</i> attribute. However, the operations are
performed only if the expression in the first entry evaluates to true.

<p>{des10:10} The condition is an expression in the usual infix form.
There are three data types supported:
<ul>
	<li> integer: a sequence of digits (decimal number, not larger than 2^31-1)
	<li> string literal: a sequence of characters enclosed in double quotes; a double quote can be added using the usual \" form, and \\ is taken as a single backslash
	<li> constant: an id or id.id that is going to be substituted with the value of the named constant; the value is always a string
</ul>

<p>{des10:11} The following binary operators are defined for strings (where each
oparand is a string literal or a constant):
<ul>
	<li> <b>str == str</b> is 1 if the two strings match (case sensitive), 0 otherwise; the result is an integer
	<li> <b>str != str</b> is 0 if the two strings match (case sensitive), 1 otherwise; the result is an integer
</ul>


<p>{des10:12} The following binary operators are defined for integers, with
their usual meaning and precedence:
<ul>
	<li> == means equal
	<li> != means not-equal
	<li> &lt;
	<li> &gt;
	<li> &lt;=
	<li> &gt;=
	<li> &amp;&amp; means logic "and"
	<li> || means logic "or"
	<li> -, +, *, / are the usual subtract, add, multiply, divide
	<li> % is the remainder of an integer division
</ul>

<p>{des10:13} The following non-binary operators are defined for integers, with
their usual meaning and precedence:
<ul>
	<li> ! is the unary "not" operator
	<li> - is the unary minus
</ul>

<p>{des10:14} Parenthesis affects the precedence the usual way.


<h2 id="des10.2"> 10.2. Stances for conditinal forge </h2>
<p>{des10:15} One of the factors that can be used in conditional forge is
<i>project stance</i>, which is a collection of project build options: when
the same project can be used to produce differently configured boards/circuits,
stance values determine which option is currently displayed or exported.

<p>{des10:16} Each stance has a name and an arbitrary textual value. Optionally
the implementation may provide a list of possible values. All values are
invented by the user.

<p>{des10:17} The implementation is free to define stance names but the
following three must be supported:
<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> standard stances
<tr><th> name      <th> usage provisions 
<tr><td> model     <td> for the simplest cases, this single stance selects which one of the build configurations is used (e.g. which model or variant of the board is made)
<tr><td> sub_major <td> major subtype; if there's a build option that is orthogonal to the model, this stance should be used
<tr><td> sub_minor <td> minor subtype; if there's a build option that is orthogonal to both the model and the sub_major stances, this stance should be used
</table>

<h2 id="des10.3"> 10.3. Standard id.id substitution </h2>
<p>{des10:18} The implementation must support at least the following id.id
substitutions:
<ul>
	<li> <b>view.name</b> is the name of the current view
	<li> <b>stance.model</b> is the current value of the project stance called model
	<li> <b>stance.sub_major</b> is the current value of the project stance called sub_major
	<li> <b>stance.sub_minor</b> is the current value of the project stance called sub_minor
</ul>
