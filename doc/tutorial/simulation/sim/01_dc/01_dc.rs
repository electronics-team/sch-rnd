ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=LnNTcUkV1CIzG7F2EXUAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.3 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAY;
				x=-24000; y=-12000;
				li:objects {
					ha:line.1 { x1=44000; y1=116000; x2=52000; y2=116000; stroke=wire; }
					ha:text.4 { x1=48000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.8 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=28000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R1
					role=symbol
					spice/prefix=R
					value=2.2k
				}
			}
			ha:group.14 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=52000; y=100000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R2
					role=symbol
					spice/prefix=R
					value=1k
				}
			}
			ha:group.24 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=24000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.37 {
				uuid=XkMAnbHX1rIO2D9kjFAAAAAn;
				x=-24000; y=-12000;
				li:objects {
					ha:line.2 { x1=76000; y1=112000; x2=76000; y2=116000; stroke=wire; }
					ha:line.3 { x1=76000; y1=116000; x2=76000; y2=116000; stroke=junction; }
					ha:text.4 { x1=84000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
					ha:line.5 { x1=72000; y1=116000; x2=88000; y2=116000; stroke=wire; }
					ha:line.6 { x1=88000; y1=116000; x2=88000; y2=92000; stroke=wire; }
					ha:line.7 { x1=88000; y1=92000; x2=92000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					name=out1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.40 {
				uuid=XkMAnbHX1rIO2D9kjFAAAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=52000; y=72000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=XkMAnbHX1rIO2D9kjFAAAAAs; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=XkMAnbHX1rIO2D9kjFAAAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R3
					role=symbol
					spice/prefix=R
					value=1k
				}
			}
			ha:group.42 {
				uuid=XkMAnbHX1rIO2D9kjFAAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=52000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=XkMAnbHX1rIO2D9kjFAAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.43 {
				uuid=XkMAnbHX1rIO2D9kjFAAAAAy;
				x=-24000; y=-16000;
				li:objects {
					ha:line.1 { x1=76000; y1=64000; x2=76000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.49 {
				uuid=XkMAnbHX1rIO2D9kjFAAAAAz;
				x=-24000; y=-12000;
				li:objects {
					ha:line.1 { x1=76000; y1=84000; x2=76000; y2=92000; stroke=wire; }
					ha:line.2 { x1=76000; y1=88000; x2=92000; y2=88000; stroke=wire; }
					ha:line.3 { x1=76000; y1=88000; x2=76000; y2=88000; stroke=junction; }
					ha:text.4 { x1=84000; y1=88000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.53 {
				li:conn {
					/2/8/2/1
					/2/3/1
				}
			}
			ha:connection.57 {
				li:conn {
					/2/8/1/1
					/2/37/5
				}
			}
			ha:connection.58 {
				li:conn {
					/2/37/2
					/2/14/2/1
				}
			}
			ha:connection.59 {
				li:conn {
					/2/43/1
					/2/40/1/1
				}
			}
			ha:connection.60 {
				li:conn {
					/2/43/1
					/2/42/1/1
				}
			}
			ha:connection.61 {
				li:conn {
					/2/49/1
					/2/14/1/1
				}
			}
			ha:connection.62 {
				li:conn {
					/2/49/1
					/2/40/2/1
				}
			}
			ha:group.63 {
				uuid=YwPM9NEZm6Skk0Cu7xAAAAAe; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAb;
				x=16000; y=100000; mirx=1;
				li:objects {
					ha:text.1 { x1=0; y1=-6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAf; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAc;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAg; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAd;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(1,2)
					name=CN1
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.64 {
				uuid=YwPM9NEZm6Skk0Cu7xAAAAAp; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAl;
				x=72000; y=72000;
				li:objects {
					ha:text.1 { x1=0; y1=-6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAq; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAm;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAr; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAn;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAs; src_uuid=YwPM9NEZm6Skk0Cu7xAAAAAo;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(3,1)
					name=CN2
					role=symbol
					spice/omit=yes
				}
			}
			ha:connection.65 {
				li:conn {
					/2/63/3/1
					/2/3/1
				}
			}
			ha:group.66 {
				uuid=YwPM9NEZm6Skk0Cu7xAAAAAt;
				li:objects {
					ha:line.1 { x1=20000; y1=100000; x2=24000; y2=100000; stroke=wire; }
					ha:line.2 { x1=24000; y1=100000; x2=24000; y2=48000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.67 {
				li:conn {
					/2/66/1
					/2/63/2/1
				}
			}
			ha:connection.69 {
				li:conn {
					/2/37/7
					/2/64/4/1
				}
			}
			ha:connection.70 {
				li:conn {
					/2/49/2
					/2/64/3/1
				}
			}
			ha:group.71 {
				uuid=YwPM9NEZm6Skk0Cu7xAAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=64000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=YwPM9NEZm6Skk0Cu7xAAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.72 {
				uuid=YwPM9NEZm6Skk0Cu7xAAAAAy;
				li:objects {
					ha:line.1 { x1=64000; y1=48000; x2=64000; y2=72000; stroke=wire; }
					ha:line.2 { x1=64000; y1=72000; x2=68000; y2=72000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.73 {
				li:conn {
					/2/72/1
					/2/71/1/1
				}
			}
			ha:connection.74 {
				li:conn {
					/2/72/2
					/2/64/2/1
				}
			}
			ha:connection.75 {
				li:conn {
					/2/66/2
					/2/24/1/1
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={high level SIM: DC operating point}
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
