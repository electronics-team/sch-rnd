ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=9bwyX4hVlbhQseMSDAAAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.13 {
				uuid=39gOSGd8aSMOm1wdWfIAAAAF;
				x=44000; y=96000;
				li:objects {
					ha:group.1 {
						uuid=39gOSGd8aSMOm1wdWfIAAAAG; src_uuid=9bwyX4hVlbhQseMSDAAAAAAD;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-8000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in1
							role=terminal
							spice/bridge/model=bridge_adc_ttl
							spice/pinnum=[1]
						}
					}
					ha:group.2 {
						uuid=39gOSGd8aSMOm1wdWfIAAAAH; src_uuid=9bwyX4hVlbhQseMSDAAAAAAE;
						x=0; y=-8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-8000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in2
							role=terminal
							spice/bridge/model=bridge_adc_ttl
							spice/pinnum=[1]
						}
					}
					ha:group.3 {
						uuid=39gOSGd8aSMOm1wdWfIAAAAI; src_uuid=9bwyX4hVlbhQseMSDAAAAAAF;
						x=24000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							spice/bridge/model=bridge_dac_ttl
							spice/pinnum=2
						}
					}
					ha:arc.4 { cx=1000; cy=-4000; r=8000; sang=-38.500000; dang=77.000000; stroke=sym-decor; }
					ha:line.5 { x1=7250; y1=1000; x2=16000; y2=1000; stroke=sym-decor; }
					ha:arc.6 { cx=2500; cy=-13750; r=20000; sang=29.200000; dang=18.300000; stroke=sym-decor; }
					ha:line.7 { x1=7250; y1=-9000; x2=16000; y2=-9000; stroke=sym-decor; }
					ha:arc.8 { cx=2500; cy=5750; r=20000; sang=-29.200000; dang=-18.300000; stroke=sym-decor; }
					ha:arc.9 { cx=0; cy=-4000; r=8000; sang=-38.500000; dang=77.000000; stroke=sym-decor; }
					ha:text.10 { x1=8000; y1=2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					device=xor
					name=U1
					role=symbol
					spice/model_card={.model d_xor1 d_xor (rise_delay=1.0e-6 fall_delay=2.0e-6
+ input_load=1.0e-12) }
					spice/prefix=A
				}
			}
			ha:group.14 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAABx;
				x=16000; y=80000; rot=270.000000; mirx=1;
				li:objects {
					ha:arc.1 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:arc.2 { cx=8000; cy=0; r=2000; sang=0.000000; dang=180.000000; stroke=sym-decor; }
					ha:arc.3 { cx=12000; cy=0; r=2000; sang=180.000000; dang=180.000000; stroke=sym-decor; }
					ha:group.4 {
						uuid=mHn79ppCTFm1QBmauQ0AAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAABy;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.5 {
						uuid=mHn79ppCTFm1QBmauQ0AAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAABz;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.6 { x1=8000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V1
					li:portmap {
						{P->spice/pinnum=1}
						{N->spice/pinnum=2}
					}
					role=symbol
					spice/params=PULSE (0 5 1 0.3 0.3 2 10)
					spice/prefix=V
				}
			}
			ha:group.15 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAs; src_uuid=LnNTcUkV1CIzG7F2EXUAAAAZ;
				x=-24000; y=-28000;
				li:objects {
					ha:line.1 { x1=40000; y1=88000; x2=40000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.17 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=16000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=mHn79ppCTFm1QBmauQ0AAAAu; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.19 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAv; src_uuid=iNOQfJpO6hT/HFDFGjoAAABx;
				x=36000; y=80000; rot=270.000000; mirx=1;
				li:objects {
					ha:arc.1 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:arc.2 { cx=8000; cy=0; r=2000; sang=0.000000; dang=180.000000; stroke=sym-decor; }
					ha:arc.3 { cx=12000; cy=0; r=2000; sang=180.000000; dang=180.000000; stroke=sym-decor; }
					ha:group.4 {
						uuid=mHn79ppCTFm1QBmauQ0AAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABy;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.5 {
						uuid=mHn79ppCTFm1QBmauQ0AAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABz;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.6 { x1=8000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V2
					li:portmap {
						{P->spice/pinnum=1}
						{N->spice/pinnum=2}
					}
					role=symbol
					spice/params=PULSE (0 5 2 0.3 0.3 2 4)
					spice/prefix=V
				}
			}
			ha:group.20 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAy; src_uuid=LnNTcUkV1CIzG7F2EXUAAAAZ;
				x=-4000; y=-28000;
				li:objects {
					ha:line.1 { x1=40000; y1=88000; x2=40000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=mHn79ppCTFm1QBmauQ0AAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=36000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=mHn79ppCTFm1QBmauQ0AAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.24 {
				uuid=mHn79ppCTFm1QBmauQ0AAAA1;
				x=-40000; y=-28000;
				li:objects {
					ha:line.1 { x1=76000; y1=108000; x2=76000; y2=116000; stroke=wire; }
					ha:line.2 { x1=76000; y1=116000; x2=84000; y2=116000; stroke=wire; }
					ha:text.3 { x1=76000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=input2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.27 {
				uuid=mHn79ppCTFm1QBmauQ0AAAA2;
				x=-40000; y=-28000;
				li:objects {
					ha:line.1 { x1=56000; y1=108000; x2=56000; y2=124000; stroke=wire; }
					ha:line.2 { x1=56000; y1=124000; x2=84000; y2=124000; stroke=wire; }
					ha:text.3 { x1=76000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=input1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.30 {
				uuid=mHn79ppCTFm1QBmauQ0AAAA3;
				x=-52000; y=-28000;
				li:objects {
					ha:line.1 { x1=120000; y1=120000; x2=124000; y2=120000; stroke=wire; }
					ha:text.2 { x1=124000; y1=120000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=output
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.32 {
				uuid=Xb4Ih/E1Q5gK2yySOvUAAAAz; src_uuid=TeGEOMuew6iCb2kzckAAAAAD;
				x=16000; y=116000;
				li:objects {
					ha:text.1 { x1=2000; y1=-4000; dyntext=0; stroke=sym-decor; text=raw spice; }
					ha:text.2 { x1=2000; y1=-8000; dyntext=0; stroke=sym-decor; text=command; }
					ha:polygon.3 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=-10000; }
							ha:line { x1=0; y1=-10000; x2=12000; y2=-10000; }
							ha:line { x1=12000; y1=-10000; x2=12000; y2=0; }
							ha:line { x1=12000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
				}
				ha:attrib {
					-sym-comment={ Fill in spice/command and use export_spice (e.g. the spice_raw view) to get that string exported at the end of the spice netlist file. }
					-sym-copyright=(C) 2023 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					role=symbol
					spice/command={tran 10ms 6s
plot v(input1)+0.04 v(input2)+0.08 v(output) xlimit 0 6s}
				}
			}
			ha:group.33 {
				uuid=GtwUPhB2CW+7+ykXMBQAAAAc; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=72000; y=92000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GtwUPhB2CW+7+ykXMBQAAAAd; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GtwUPhB2CW+7+ykXMBQAAAAe; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R1
					role=symbol
					value=100k
				}
			}
			ha:group.35 {
				uuid=GtwUPhB2CW+7+ykXMBQAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=72000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=GtwUPhB2CW+7+ykXMBQAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.36 {
				uuid=GtwUPhB2CW+7+ykXMBQAAAAj;
				x=-60000; y=-28000;
				li:objects {
					ha:line.1 { x1=132000; y1=100000; x2=132000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.42 {
				li:conn {
					/2/15/1
					/2/14/4/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/17/1/1
					/2/15/1
				}
			}
			ha:connection.44 {
				li:conn {
					/2/20/1
					/2/19/4/1
				}
			}
			ha:connection.45 {
				li:conn {
					/2/22/1/1
					/2/20/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/24/1
					/2/19/5/1
				}
			}
			ha:connection.48 {
				li:conn {
					/2/27/1
					/2/14/5/1
				}
			}
			ha:connection.54 {
				li:conn {
					/2/13/1/1
					/2/27/2
				}
			}
			ha:connection.55 {
				li:conn {
					/2/13/2/1
					/2/24/2
				}
			}
			ha:connection.60 {
				li:conn {
					/2/30/1
					/2/13/3/1
				}
			}
			ha:connection.61 {
				li:conn {
					/2/33/2/1
					/2/30/1
				}
			}
			ha:connection.62 {
				li:conn {
					/2/36/1
					/2/33/1/1
				}
			}
			ha:connection.63 {
				li:conn {
					/2/36/1
					/2/35/1/1
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={SIM raw spice: mixed digital+analog}
		}
	}
}
