ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEk;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAD5;
						loclib_name=2n7002_sot23
						li:objects {
						}
						ha:attrib {
							footprint=sot23
							li:portmap {
								{G->pcb/pinnum=1}
								{S->pcb/pinnum=2}
								{D->pcb/pinnum=3}
							}
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAD6;
						loclib_name=irf510_to220
						li:objects {
						}
						ha:attrib {
							footprint=TO220
							li:portmap {
								{G->pcb/pinnum=1}
								{S->pcb/pinnum=3}
								{D->pcb/pinnum=2}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=iNOQfJpO6hT/HFDFGjoAAAEj;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAD7;
				x=132000; y=68000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAD8;
						x=12000; y=12000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=D
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAD9;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=G
							role=terminal
						}
					}
					ha:text.3 { x1=20000; y1=-12000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=11000; cy=3000; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=9000; y1=-1000; x2=9000; y2=1000; stroke=sym-decor; }
					ha:line.8 { x1=9000; y1=2000; x2=9000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=5000; x2=9000; y2=7000; stroke=sym-decor; }
					ha:line.10 { x1=9000; y1=3000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.11 { x1=9000; y1=0; x2=12000; y2=0; stroke=sym-decor; }
					ha:line.12 { x1=9000; y1=6000; x2=12000; y2=6000; stroke=sym-decor; }
					ha:line.13 { x1=12000; y1=6000; x2=12000; y2=8000; stroke=sym-decor; }
					ha:line.14 { x1=12000; y1=-4000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.15 { x1=13000; y1=3000; x2=15000; y2=3000; stroke=sym-decor; }
					ha:line.16 { x1=15000; y1=3000; x2=14000; y2=4000; stroke=sym-decor; }
					ha:line.17 { x1=14000; y1=4000; x2=13000; y2=3000; stroke=sym-decor; }
					ha:line.18 { x1=13000; y1=4000; x2=15000; y2=4000; stroke=sym-decor; }
					ha:line.19 { x1=12000; y1=7000; x2=14000; y2=7000; stroke=sym-decor; }
					ha:line.20 { x1=14000; y1=-1000; x2=12000; y2=-1000; stroke=sym-decor; }
					ha:line.21 { x1=8000; y1=7000; x2=8000; y2=0; stroke=sym-decor; }
					ha:polygon.22 {
						li:outline {
							ha:line { x1=10000; y1=4000; x2=9000; y2=3000; }
							ha:line { x1=9000; y1=3000; x2=10000; y2=2000; }
							ha:line { x1=10000; y1=2000; x2=10000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.23 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAD+;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=S
							role=terminal
						}
					}
					ha:line.24 { x1=14000; y1=-1000; x2=14000; y2=3000; stroke=sym-decor; }
					ha:line.25 { x1=14000; y1=4000; x2=14000; y2=7000; stroke=sym-decor; }
				}
				ha:attrib {
					devmap=2n7002_sot23
					name=Q1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.7 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAD/;
				x=116000; y=64000; mirx=1;
				li:objects {
					ha:text.1 { x1=8000; y1=-4000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEA;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEB;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEC;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN1
					role=symbol
				}
			}
			ha:group.11 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAED;
				x=144000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEE;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.13 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEF;
				x=146000; y=94000; rot=90.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEG;
						li:objects {
							ha:line.1 { x1=18000; y1=2000; x2=15000; y2=2000; stroke=term-primary; }
						}
						ha:attrib {
							pinlabel=2
							pinnumber=2
							pinseq=2
							pintype=pas
							ha:role = { value=terminal; prio=0; }
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEH;
						li:objects {
							ha:line.1 { x1=0; y1=2000; x2=3000; y2=2000; stroke=term-primary; }
						}
						ha:attrib {
							pinlabel=1
							pinnumber=1
							pinseq=1
							pintype=pas
							ha:role = { value=terminal; prio=0; }
						}
					}
					ha:polygon.3 {
						li:outline {
							ha:line { x1=3000; y1=0; x2=3000; y2=4000; }
							ha:line { x1=3000; y1=4000; x2=15000; y2=4000; }
							ha:line { x1=15000; y1=4000; x2=15000; y2=0; }
							ha:line { x1=15000; y1=0; x2=3000; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:text.4 { x1=8000; y1=4000; dyntext=1; stroke=sym-decor; text=%../A.refdes%; floater=1; }
				}
				ha:attrib {
					device=RESISTOR
					refdes=R1
					ha:role = { value=symbol; prio=0; }
				}
			}
			ha:group.15 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEI;
				x=144000; y=140000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEJ;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.20 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEK;
				li:objects {
					ha:line.1 { x1=144000; y1=140000; x2=144000; y2=112000; stroke=wire; }
					ha:line.2 { x1=168000; y1=122000; x2=144000; y2=122000; stroke=wire; }
					ha:line.3 { x1=144000; y1=122000; x2=144000; y2=122000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.24 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEL;
				x=168000; y=122000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEM;
						x=12000; y=12000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=D
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEN;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=G
							role=terminal
						}
					}
					ha:text.3 { x1=8000; y1=-30000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=11000; cy=3000; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=9000; y1=-1000; x2=9000; y2=1000; stroke=sym-decor; }
					ha:line.8 { x1=9000; y1=2000; x2=9000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=5000; x2=9000; y2=7000; stroke=sym-decor; }
					ha:line.10 { x1=9000; y1=3000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.11 { x1=9000; y1=0; x2=12000; y2=0; stroke=sym-decor; }
					ha:line.12 { x1=9000; y1=6000; x2=12000; y2=6000; stroke=sym-decor; }
					ha:line.13 { x1=12000; y1=6000; x2=12000; y2=8000; stroke=sym-decor; }
					ha:line.14 { x1=12000; y1=-4000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.15 { x1=13000; y1=3000; x2=15000; y2=3000; stroke=sym-decor; }
					ha:line.16 { x1=15000; y1=3000; x2=14000; y2=4000; stroke=sym-decor; }
					ha:line.17 { x1=14000; y1=4000; x2=13000; y2=3000; stroke=sym-decor; }
					ha:line.18 { x1=13000; y1=4000; x2=15000; y2=4000; stroke=sym-decor; }
					ha:line.19 { x1=12000; y1=7000; x2=14000; y2=7000; stroke=sym-decor; }
					ha:line.20 { x1=14000; y1=-1000; x2=12000; y2=-1000; stroke=sym-decor; }
					ha:line.21 { x1=8000; y1=7000; x2=8000; y2=0; stroke=sym-decor; }
					ha:polygon.22 {
						li:outline {
							ha:line { x1=10000; y1=4000; x2=9000; y2=3000; }
							ha:line { x1=9000; y1=3000; x2=10000; y2=2000; }
							ha:line { x1=10000; y1=2000; x2=10000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.23 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEO;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=S
							role=terminal
						}
					}
					ha:line.24 { x1=14000; y1=-1000; x2=14000; y2=3000; stroke=sym-decor; }
					ha:line.25 { x1=14000; y1=4000; x2=14000; y2=7000; stroke=sym-decor; }
				}
				ha:attrib {
					devmap=irf510_to220
					name=Q2
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.26 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEP;
				li:objects {
					ha:line.1 { x1=180000; y1=114000; x2=180000; y2=52000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.28 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEQ;
				x=180000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAER;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.31 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAES;
				x=196000; y=138000;
				li:objects {
					ha:text.1 { x1=8000; y1=-4000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAET;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEU;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN2
					role=symbol
				}
			}
			ha:group.32 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEV;
				li:objects {
					ha:line.1 { x1=180000; y1=134000; x2=180000; y2=142000; stroke=wire; }
					ha:line.2 { x1=180000; y1=142000; x2=192000; y2=142000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.35 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEW;
				li:objects {
					ha:line.1 { x1=192000; y1=138000; x2=190000; y2=138000; stroke=wire; }
					ha:line.2 { x1=190000; y1=138000; x2=190000; y2=132000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.37 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEX;
				x=190000; y=132000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEY;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.39 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEZ;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.42 {
				li:conn {
					/2/20/1
					/2/13/1/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/20/1
					/2/15/1/1
				}
			}
			ha:connection.44 {
				li:conn {
					/2/24/2/1
					/2/20/2
				}
			}
			ha:connection.45 {
				li:conn {
					/2/26/1
					/2/24/23/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/28/1/1
					/2/26/1
				}
			}
			ha:connection.47 {
				li:conn {
					/2/32/1
					/2/24/1/1
				}
			}
			ha:connection.48 {
				li:conn {
					/2/32/2
					/2/31/3/1
				}
			}
			ha:connection.49 {
				li:conn {
					/2/35/1
					/2/31/2/1
				}
			}
			ha:connection.50 {
				li:conn {
					/2/37/1/1
					/2/35/2
				}
			}
			ha:group.51 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEa;
				li:objects {
					ha:line.1 { x1=120000; y1=72000; x2=124000; y2=72000; stroke=wire; }
					ha:line.2 { x1=124000; y1=72000; x2=124000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.52 {
				li:conn {
					/2/51/1
					/2/7/4/1
				}
			}
			ha:group.53 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEb;
				li:objects {
					ha:line.1 { x1=120000; y1=64000; x2=124000; y2=64000; stroke=wire; }
					ha:line.2 { x1=124000; y1=64000; x2=124000; y2=56000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.54 {
				li:conn {
					/2/53/1
					/2/7/2/1
				}
			}
			ha:group.55 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEc;
				x=124000; y=56000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEd;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.56 {
				li:conn {
					/2/55/1/1
					/2/53/2
				}
			}
			ha:group.57 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEe;
				x=124000; y=80000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAEf;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.58 {
				li:conn {
					/2/57/1/1
					/2/51/2
				}
			}
			ha:group.59 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEg;
				li:objects {
					ha:line.1 { x1=120000; y1=68000; x2=132000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.60 {
				li:conn {
					/2/59/1
					/2/1/2/1
				}
			}
			ha:connection.61 {
				li:conn {
					/2/59/1
					/2/7/3/1
				}
			}
			ha:group.62 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEh;
				li:objects {
					ha:line.1 { x1=144000; y1=52000; x2=144000; y2=60000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.63 {
				li:conn {
					/2/62/1
					/2/1/23/1
				}
			}
			ha:connection.64 {
				li:conn {
					/2/62/1
					/2/11/1/1
				}
			}
			ha:group.65 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEi;
				li:objects {
					ha:line.1 { x1=144000; y1=94000; x2=144000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.66 {
				li:conn {
					/2/65/1
					/2/1/1/1
				}
			}
			ha:connection.67 {
				li:conn {
					/2/65/1
					/2/13/2/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=sch-rnd devmap example
		}
	}
}
