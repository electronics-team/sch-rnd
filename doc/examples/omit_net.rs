ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=E4wBSbBPnQTyLrlukNMAAAAV;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAAAW; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=E4wBSbBPnQTyLrlukNMAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.16 {
				uuid=E4wBSbBPnQTyLrlukNMAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=124000; y=104000; miry=1;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=E4wBSbBPnQTyLrlukNMAAABD; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=E4wBSbBPnQTyLrlukNMAAABE; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=E4wBSbBPnQTyLrlukNMAAABF; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=E4wBSbBPnQTyLrlukNMAAABG; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-slot=1
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=U1
					role=symbol
				}
			}
			ha:group.22 {
				uuid=E4wBSbBPnQTyLrlukNMAAABM; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=104000; y=136000; miry=1;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABN; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=E4wBSbBPnQTyLrlukNMAAABO; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R1
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=47k
				}
			}
			ha:group.23 {
				uuid=E4wBSbBPnQTyLrlukNMAAABP; src_uuid=E4wBSbBPnQTyLrlukNMAAAAp;
				x=16000; y=244000; miry=1;
				li:objects {
					ha:line.1 { x1=84000; y1=136000; x2=76000; y2=136000; stroke=wire; }
					ha:line.2 { x1=76000; y1=136000; x2=76000; y2=108000; stroke=wire; }
					ha:line.4 { x1=68000; y1=108000; x2=88000; y2=108000; stroke=wire; }
					ha:line.5 { x1=76000; y1=108000; x2=76000; y2=108000; stroke=junction; }
					ha:text.6 { x1=76000; y1=112000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=opa_neg
					omit=1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.24 {
				li:conn {
					/2/23/1
					/2/16/2/1
				}
			}
			ha:connection.25 {
				li:conn {
					/2/22/2/1
					/2/23/4
				}
			}
			ha:group.26 {
				uuid=E4wBSbBPnQTyLrlukNMAAABQ; src_uuid=E4wBSbBPnQTyLrlukNMAAAAq;
				x=16000; y=244000; miry=1;
				li:objects {
					ha:line.1 { x1=108000; y1=108000; x2=124000; y2=108000; stroke=wire; }
					ha:line.2 { x1=124000; y1=108000; x2=124000; y2=140000; stroke=wire; }
					ha:line.4 { x1=108000; y1=140000; x2=148000; y2=140000; stroke=wire; }
					ha:line.5 { x1=124000; y1=140000; x2=124000; y2=140000; stroke=junction; }
					ha:text.6 { x1=132000; y1=140000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.27 {
				li:conn {
					/2/26/1
					/2/22/1/1
				}
			}
			ha:connection.28 {
				li:conn {
					/2/16/3/1
					/2/26/4
				}
			}
			ha:group.29 {
				uuid=E4wBSbBPnQTyLrlukNMAAABR; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=64000; y=136000; miry=1;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABS; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=E4wBSbBPnQTyLrlukNMAAABT; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R2
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=10k
				}
			}
			ha:group.30 {
				uuid=E4wBSbBPnQTyLrlukNMAAABY; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=124000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.31 {
				uuid=E4wBSbBPnQTyLrlukNMAAABa;
				li:objects {
					ha:line.1 { x1=112000; y1=112000; x2=112000; y2=120000; stroke=wire; }
					ha:line.2 { x1=112000; y1=120000; x2=124000; y2=120000; stroke=wire; }
					ha:line.3 { x1=124000; y1=120000; x2=124000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.32 {
				li:conn {
					/2/31/1
					/2/16/10/1
				}
			}
			ha:connection.33 {
				li:conn {
					/2/31/3
					/2/30/1/1
				}
			}
			ha:group.34 {
				uuid=E4wBSbBPnQTyLrlukNMAAABf; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=124000; y=96000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABg; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.37 {
				uuid=E4wBSbBPnQTyLrlukNMAAABi;
				li:objects {
					ha:line.1 { x1=112000; y1=96000; x2=112000; y2=92000; stroke=wire; }
					ha:line.2 { x1=112000; y1=92000; x2=124000; y2=92000; stroke=wire; }
					ha:line.3 { x1=124000; y1=92000; x2=124000; y2=96000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.38 {
				li:conn {
					/2/37/1
					/2/16/11/1
				}
			}
			ha:connection.39 {
				li:conn {
					/2/37/3
					/2/34/1/1
				}
			}
			ha:group.40 {
				uuid=E4wBSbBPnQTyLrlukNMAAABj;
				li:objects {
					ha:line.1 { x1=100000; y1=100000; x2=92000; y2=100000; stroke=wire; }
					ha:line.2 { x1=92000; y1=100000; x2=92000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.41 {
				li:conn {
					/2/40/1
					/2/16/1/1
				}
			}
			ha:group.42 {
				uuid=E4wBSbBPnQTyLrlukNMAAABm; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=92000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAABn; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.43 {
				li:conn {
					/2/42/1/1
					/2/40/2
				}
			}
			ha:connection.44 {
				li:conn {
					/2/23/4
					/2/29/1/1
				}
			}
			ha:group.45 {
				uuid=E4wBSbBPnQTyLrlukNMAAABo;
				li:objects {
					ha:line.2 { x1=48000; y1=136000; x2=64000; y2=136000; stroke=wire; }
					ha:text.3 { x1=56000; y1=136000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.46 {
				li:conn {
					/2/29/2/1
					/2/45/2
				}
			}
			ha:group.47 {
				uuid=E4wBSbBPnQTyLrlukNMAAABx; src_uuid=E4wBSbBPnQTyLrlukNMAAABt;
				x=168000; y=100000;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=E4wBSbBPnQTyLrlukNMAAABy; src_uuid=E4wBSbBPnQTyLrlukNMAAABu;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=E4wBSbBPnQTyLrlukNMAAABz; src_uuid=E4wBSbBPnQTyLrlukNMAAABv;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=E4wBSbBPnQTyLrlukNMAAAB0; src_uuid=E4wBSbBPnQTyLrlukNMAAABw;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN2
					role=symbol
				}
			}
			ha:group.48 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB1; src_uuid=E4wBSbBPnQTyLrlukNMAAABt;
				x=44000; y=132000; mirx=1;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=E4wBSbBPnQTyLrlukNMAAAB2; src_uuid=E4wBSbBPnQTyLrlukNMAAABu;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=E4wBSbBPnQTyLrlukNMAAAB3; src_uuid=E4wBSbBPnQTyLrlukNMAAABv;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=E4wBSbBPnQTyLrlukNMAAAB4; src_uuid=E4wBSbBPnQTyLrlukNMAAABw;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN1
					role=symbol
				}
			}
			ha:connection.49 {
				li:conn {
					/2/45/2
					/2/48/3/1
				}
			}
			ha:connection.50 {
				li:conn {
					/2/26/4
					/2/47/3/1
				}
			}
			ha:group.51 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB5;
				li:objects {
					ha:line.1 { x1=164000; y1=108000; x2=160000; y2=108000; stroke=wire; }
					ha:line.2 { x1=160000; y1=108000; x2=160000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.52 {
				li:conn {
					/2/51/1
					/2/47/4/1
				}
			}
			ha:group.53 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB6;
				li:objects {
					ha:line.1 { x1=164000; y1=100000; x2=160000; y2=100000; stroke=wire; }
					ha:line.2 { x1=160000; y1=100000; x2=160000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.54 {
				li:conn {
					/2/53/1
					/2/47/2/1
				}
			}
			ha:group.55 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB7;
				li:objects {
					ha:line.1 { x1=48000; y1=140000; x2=52000; y2=140000; stroke=wire; }
					ha:line.2 { x1=52000; y1=140000; x2=52000; y2=148000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.56 {
				li:conn {
					/2/55/1
					/2/48/4/1
				}
			}
			ha:group.57 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB8;
				li:objects {
					ha:line.1 { x1=48000; y1=132000; x2=52000; y2=132000; stroke=wire; }
					ha:line.2 { x1=52000; y1=132000; x2=52000; y2=124000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.58 {
				li:conn {
					/2/57/1
					/2/48/2/1
				}
			}
			ha:group.59 {
				uuid=E4wBSbBPnQTyLrlukNMAAAB/; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=52000; y=124000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAACA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.60 {
				li:conn {
					/2/59/1/1
					/2/57/2
				}
			}
			ha:group.61 {
				uuid=E4wBSbBPnQTyLrlukNMAAACB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=160000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAACC; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.62 {
				li:conn {
					/2/61/1/1
					/2/53/2
				}
			}
			ha:group.63 {
				uuid=E4wBSbBPnQTyLrlukNMAAACF; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=160000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAACG; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.64 {
				li:conn {
					/2/63/1/1
					/2/51/2
				}
			}
			ha:group.65 {
				uuid=E4wBSbBPnQTyLrlukNMAAACH; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=52000; y=148000;
				li:objects {
					ha:group.1 {
						uuid=E4wBSbBPnQTyLrlukNMAAACI; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.66 {
				li:conn {
					/2/65/1/1
					/2/55/2
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=omit network opa_neg example
		}
	}
}
