tEDAx v1


# Mixed: verbose components, compact, list-like networks
#
# This file combines small2.tdx and small3.tdx syntax, read those
# files for semantics
#
# Advantage: verbose syntax for components where there's likely more
#            attributes, compact, table-like or netlist-like syntax for
#            nets
#
# Example: imaginary CPU with two srams, an IO chip and an extension
# connector
# bypass caps and power/gnd connections are specified at components
# because they are more of a component-property than a network-property

begin cschem_acomp U2
	footprint dip(24)
	device SRAM
	value TMS4016
	- 24 Vcc
	- 12 gnd
	- 18 CSsram1
end cschem_acomp

begin cschem_acomp U3
	footprint dip(24)
	device SRAM
	value TMS4016
	- 24 Vcc
	- 12 gnd
	- 18 CSsram2
end cschem_acomp

begin cschem_acomp U1
	footprint tqfp(32)
	device CPU
	- 11 Vcc
	- 12 gnd
	- 25 Vcc
	- 27 gnd
	- 28 reset
end cschem_acomp

begin cschem_acomp U4
	footprint dip(24)
	device IOports
	- 22 CSio
	- 23 Vcc
	- 24 gnd
end cschem_acomp

begin cschem_acomp CONN1
	footprint connector(16,2)
	comment extension bus connector
	- 1 Vcc
	- 17 Vcc
	- 2 gnd
	- 18 gnd
end cschem_acomp

# U5 is specified on a graphical sheet, it controls the chip select
# of U2, U3 and U4 using networks A11 and A12 as input, CSsram1, CSsram2
# and CSio as output.

begin cschem_acompact
	net gnd comment=use\ poly
	net vcc comment=use\ poly

	conn A0  U2=8 U3=8 U1=1 U4=11 CONN1=3
	conn A1  U2=7 U3=7 U1=2 U4=12 CONN1=4
	conn A2  U2=6 U3=6 U1=3 U4=13 CONN1=5
	conn A3  U2=5 U3=5 U1=4 U4=14 CONN1=6
	conn A4  U2=4 U3=4 U1=5 U4=15 CONN1=7
	conn A5  U2=3 U3=3 U1=6 U4=16 CONN1=8
	conn A6  U2=2 U3=2 U1=7 U4=17 CONN1=9
	conn A7  U2=1 U3=1 U1=8 U4=18 CONN1=10
	conn A8  U2=23 U3=23 U1=9 U4=19 CONN1=11
	conn A9  U2=22 U3=22 U1=10 U4=20 CONN1=12
	conn A10 U2=19 U3=19 U1=13 U4=21 CONN1=13
	conn A11 U1=14 CONN1=14
	conn A12 U1=15 CONN1=15
	conn DQ1 U2=9 U3=9 U1=16 U4=10 CONN1=19
	conn DQ2 U2=10 U3=10 U1=17 U4=9 CONN1=20
	conn DQ3 U2=11 U3=11 U1=18 U4=8 CONN1=21
	conn DQ4 U2=13 U3=13 U1=19 U4=7 CONN1=22
	conn DQ5 U2=14 U3=14 U1=20 U4=6 CONN1=23
	conn DQ6 U2=15 U3=15 U1=21 U4=5 CONN1=24
	conn DQ7 U2=16 U3=16 U1=22 U4=4 CONN1=25
	conn DQ8 U2=17 U3=17 U1=23 U4=3 CONN1=26
	conn WR  U2=21 U3=21 U1=23 U4=1 CONN1=27
	conn RD  U2=20 U3=20 U1=23 U4=2 CONN1=28
	conn IRQ1 U1=24 CONN1=29
	conn IRQ2 U1=29 CONN1=30
	conn IRQ3 U1=30 CONN1=31
	conn IRQ4 U1=31 CONN1=32
end cschem_acompact

begin cschem_acomp C1
	footprint 1206
	device capacitor
	- 1 Vcc
	- 2 gnd
end cschem_acomp


begin cschem_acomp C2
	footprint 1206
	device capacitor
	- 1 Vcc
	- 2 gnd
end cschem_acomp

begin cschem_acomp C3
	footprint 1206
	device capacitor
	- 1 Vcc
	- 2 gnd
end cschem_acomp

begin cschem_acomp C4
	footprint 1206
	device capacitor
	- 1 Vcc
	- 2 gnd
end cschem_acomp

begin cschem_acomp C5
	footprint 1206
	device capacitor
	- 1 Vcc
	- 2 gnd
end cschem_acomp

