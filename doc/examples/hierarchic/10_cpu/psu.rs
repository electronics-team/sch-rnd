ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAw;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAx; loclib_name=led5;
						li:objects {
						}
						ha:attrib {
							device=led5
							footprint=LED5
							li:portmap {
								{C->pcb/pinnum=1}
								{A->pcb/pinnum=2}
							}
						}
					}
					ha:group.2 {
						uuid=wv2G1HNG+FBgM2e70J8AAADN; loclib_name=pol_rcy;
						li:objects {
						}
						ha:attrib {
							li:portmap {
								{P->pcb/pinnum=1}
								{N->pcb/pinnum=2}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=T7RkhKr/RbyI7kwRboIAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAL; src_uuid=iNOQfJpO6hT/HFDFGjoAAABo;
				x=88000; y=92000;
				li:objects {
					ha:text.1 { x1=0; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=8000; }
							ha:line { x1=0; y1=8000; x2=24000; y2=8000; }
							ha:line { x1=24000; y1=8000; x2=24000; y2=0; }
							ha:line { x1=24000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAM; src_uuid=iNOQfJpO6hT/HFDFGjoAAABp;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../A.name%; }
						}
						ha:attrib {
							name=in
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAN; src_uuid=iNOQfJpO6hT/HFDFGjoAAABq;
						x=24000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../A.name%; }
						}
						ha:attrib {
							name=out
							pinnum=3
							role=terminal
						}
					}
					ha:group.5 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAO; src_uuid=iNOQfJpO6hT/HFDFGjoAAABr;
						x=12000; y=0; rot=-90.000000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../A.name%; }
						}
						ha:attrib {
							name=gnd
							pinnum=2
							role=terminal
						}
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					-symbol_generator=boxsym-rnd
					device=7805
					footprint=TO220
					name=U301
					role=symbol
				}
			}
			ha:group.3 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAV; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=120000; y=88000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAW; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAX; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=capacitor; prio=31050; }
					footprint=1206
					name=C303
					role=symbol
					ha:spice/prefix = { value=C; prio=31050; }
					value=100n
				}
			}
			ha:group.4 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAe; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAk;
				x=60000; y=88000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAf; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAl;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAm;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
					ha:arc.8 { cx=34000; cy=0; r=23000; sang=167.500000; dang=25.000000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=-3000; x2=8000; y2=-3000; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-4000; x2=7000; y2=-2000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=capacitor; prio=31050; }
					devmap=pol_rcy
					footprint=rcy(200)
					name=C301
					role=symbol
					ha:spice/prefix = { value=C; prio=31050; }
					value=10u
				}
			}
			ha:group.5 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAl; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=100000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAm; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.6 {
				uuid=T7RkhKr/RbyI7kwRboIAAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAQ;
				x=168000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAu; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAR;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=T7RkhKr/RbyI7kwRboIAAAAv; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAS;
						x=-16000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:line.3 { x1=-4000; y1=0; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.4 { x1=-12000; y1=0; x2=-10000; y2=0; stroke=sym-decor; }
					ha:line.5 { x1=-10000; y1=4000; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-6000; y1=0; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.7 { x1=-10000; y1=4000; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.8 { x1=-6000; y1=4000; x2=-6000; y2=-4000; stroke=sym-decor; }
					ha:text.9 { x1=-4000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.10 { x1=-8000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.11 { x1=-8000; y1=8000; x2=-6000; y2=11000; stroke=sym-decor; }
					ha:line.12 { x1=-6000; y1=11000; x2=-7000; y2=10000; stroke=sym-decor; }
					ha:line.13 { x1=-6000; y1=11000; x2=-6517; y2=9545; stroke=sym-decor; }
					ha:line.14 { x1=-10000; y1=7000; x2=-8000; y2=10000; stroke=sym-decor; }
					ha:line.15 { x1=-8000; y1=10000; x2=-8000; y2=8000; stroke=sym-decor; }
					ha:line.16 { x1=-8303; y1=6354; x2=-6303; y2=9354; stroke=sym-decor; }
					ha:line.17 { x1=-6303; y1=9354; x2=-7303; y2=8354; stroke=sym-decor; }
					ha:line.18 { x1=-6303; y1=9354; x2=-6820; y2=7899; stroke=sym-decor; }
					ha:line.19 { x1=-10303; y1=5354; x2=-8303; y2=8354; stroke=sym-decor; }
					ha:line.20 { x1=-8303; y1=8354; x2=-8303; y2=6354; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=led5
					name=D301
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.7 {
				uuid=T7RkhKr/RbyI7kwRboIAAAA4; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=144000; y=88000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAAA5; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=T7RkhKr/RbyI7kwRboIAAAA6; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					footprint=1206
					name=R301
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=510
				}
			}
			ha:group.8 {
				uuid=T7RkhKr/RbyI7kwRboIAAAA7;
				x=0; y=-48000;
				li:objects {
					ha:line.1 { x1=100000; y1=136000; x2=100000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.11 {
				uuid=T7RkhKr/RbyI7kwRboIAAAA8;
				x=0; y=-48000;
				li:objects {
					ha:line.2 { x1=120000; y1=144000; x2=120000; y2=136000; stroke=wire; }
					ha:line.4 { x1=120000; y1=144000; x2=120000; y2=144000; stroke=junction; }
					ha:line.6 { x1=144000; y1=144000; x2=144000; y2=136000; stroke=wire; }
					ha:line.7 { x1=144000; y1=144000; x2=144000; y2=144000; stroke=junction; }
					ha:line.8 { x1=116000; y1=144000; x2=172000; y2=144000; stroke=wire; }
					ha:line.9 { x1=172000; y1=144000; x2=172000; y2=148000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.15 {
				uuid=T7RkhKr/RbyI7kwRboIAAAA/; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=120000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.16 {
				uuid=T7RkhKr/RbyI7kwRboIAAABB;
				x=0; y=-48000;
				li:objects {
					ha:line.1 { x1=120000; y1=116000; x2=120000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.19 {
				uuid=T7RkhKr/RbyI7kwRboIAAABJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=80000; y=88000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABK; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=T7RkhKr/RbyI7kwRboIAAABL; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=capacitor; prio=31050; }
					footprint=1206
					name=C302
					role=symbol
					ha:spice/prefix = { value=C; prio=31050; }
					value=100n
				}
			}
			ha:group.20 {
				uuid=T7RkhKr/RbyI7kwRboIAAABM; src_uuid=T7RkhKr/RbyI7kwRboIAAAA8;
				x=-40000; y=-48000;
				li:objects {
					ha:line.2 { x1=120000; y1=144000; x2=120000; y2=136000; stroke=wire; }
					ha:line.3 { x1=124000; y1=144000; x2=92000; y2=144000; stroke=wire; }
					ha:line.4 { x1=120000; y1=144000; x2=120000; y2=144000; stroke=junction; }
					ha:line.5 { x1=100000; y1=144000; x2=100000; y2=136000; stroke=wire; }
					ha:line.6 { x1=100000; y1=144000; x2=100000; y2=144000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=T7RkhKr/RbyI7kwRboIAAABN; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=80000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABO; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.23 {
				uuid=T7RkhKr/RbyI7kwRboIAAABP; src_uuid=T7RkhKr/RbyI7kwRboIAAABB;
				x=-40000; y=-48000;
				li:objects {
					ha:line.1 { x1=120000; y1=116000; x2=120000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.28 {
				uuid=T7RkhKr/RbyI7kwRboIAAABQ;
				x=0; y=-48000;
				li:objects {
					ha:line.1 { x1=144000; y1=116000; x2=144000; y2=112000; stroke=wire; }
					ha:line.2 { x1=144000; y1=112000; x2=152000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.31 {
				uuid=T7RkhKr/RbyI7kwRboIAAABT; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=172000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABU; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.32 {
				uuid=T7RkhKr/RbyI7kwRboIAAABV;
				x=0; y=-48000;
				li:objects {
					ha:line.1 { x1=168000; y1=112000; x2=172000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.36 {
				uuid=T7RkhKr/RbyI7kwRboIAAABW;
				x=0; y=-48000;
				li:objects {
					ha:line.1 { x1=60000; y1=116000; x2=60000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.38 {
				uuid=T7RkhKr/RbyI7kwRboIAAABZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=60000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABa; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.40 {
				uuid=T7RkhKr/RbyI7kwRboIAAABf; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=172000; y=100000;
				li:objects {
					ha:group.1 {
						uuid=T7RkhKr/RbyI7kwRboIAAABg; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.42 {
				uuid=T7RkhKr/RbyI7kwRboIAAABj; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=52000; y=96000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=Vin
					role=terminal
				}
			}
			ha:connection.44 {
				li:conn {
					/2/8/1
					/2/2/5/1
				}
			}
			ha:connection.45 {
				li:conn {
					/2/8/1
					/2/5/1/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/11/2
					/2/3/2/1
				}
			}
			ha:connection.47 {
				li:conn {
					/2/11/6
					/2/7/2/1
				}
			}
			ha:connection.48 {
				li:conn {
					/2/11/8
					/2/2/4/1
				}
			}
			ha:connection.49 {
				li:conn {
					/2/16/1
					/2/3/1/1
				}
			}
			ha:connection.50 {
				li:conn {
					/2/16/1
					/2/15/1/1
				}
			}
			ha:connection.51 {
				li:conn {
					/2/20/2
					/2/19/2/1
				}
			}
			ha:connection.52 {
				li:conn {
					/2/20/3
					/2/2/3/1
				}
			}
			ha:connection.53 {
				li:conn {
					/2/20/5
					/2/4/2/1
				}
			}
			ha:connection.54 {
				li:conn {
					/2/23/1
					/2/19/1/1
				}
			}
			ha:connection.55 {
				li:conn {
					/2/23/1
					/2/22/1/1
				}
			}
			ha:connection.56 {
				li:conn {
					/2/28/1
					/2/7/1/1
				}
			}
			ha:connection.57 {
				li:conn {
					/2/28/2
					/2/6/2/1
				}
			}
			ha:connection.58 {
				li:conn {
					/2/32/1
					/2/31/1/1
				}
			}
			ha:connection.59 {
				li:conn {
					/2/32/1
					/2/6/1/1
				}
			}
			ha:connection.60 {
				li:conn {
					/2/36/1
					/2/4/1/1
				}
			}
			ha:connection.61 {
				li:conn {
					/2/38/1/1
					/2/36/1
				}
			}
			ha:connection.62 {
				li:conn {
					/2/40/1/1
					/2/11/9
				}
			}
			ha:connection.63 {
				li:conn {
					/2/42/1
					/2/20/3
				}
			}
			ha:group.67 {
				uuid=iV6mVSrBkvVTynZQqdkAAADI;
				x=204000; y=100000;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=-12000; }
							ha:line { x1=0; y1=-12000; x2=16000; y2=-12000; }
							ha:line { x1=16000; y1=-12000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:group.2 {
						uuid=iV6mVSrBkvVTynZQqdkAAADJ; src_uuid=dp4EN5+Q1YO5vR+GW1UAAAAs;
						x=8000; y=4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vcc
							pinnum=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iV6mVSrBkvVTynZQqdkAAADK; src_uuid=dp4EN5+Q1YO5vR+GW1UAAAAs;
						x=8000; y=-16000; rot=90.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vss
							pinnum=2
							role=terminal
						}
					}
					ha:text.4 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=U1
					role=symbol
				}
			}
			ha:group.68 {
				uuid=iV6mVSrBkvVTynZQqdkAAADN; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=212000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=iV6mVSrBkvVTynZQqdkAAADO; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.69 {
				li:conn {
					/2/68/1/1
					/2/67/2/1
				}
			}
			ha:group.70 {
				uuid=iV6mVSrBkvVTynZQqdkAAADR; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=212000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=iV6mVSrBkvVTynZQqdkAAADS; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.71 {
				li:conn {
					/2/70/1/1
					/2/67/3/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=3/4
			print_page=A/4
			title=power supply
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
