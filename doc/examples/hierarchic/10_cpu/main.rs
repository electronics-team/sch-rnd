ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=k24MaEHH5dTW8BU3R+wAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.3 {
				uuid=+6YGVDtwpVypVzYJj6AAAAC0; src_uuid=Q8cSXpoK0QkYCcKy+asAAABC;
				x=56000; y=144000;
				li:objects {
					ha:group.1 {
						uuid=+6YGVDtwpVypVzYJj6AAAAC1; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=term_rx
							role=terminal
						}
					}
					ha:group.2 {
						uuid=+6YGVDtwpVypVzYJj6AAAAC2; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=term_tx
							role=terminal
						}
					}
					ha:group.3 {
						uuid=+6YGVDtwpVypVzYJj6AAAAC3; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=inv_rx
							role=terminal
						}
					}
					ha:group.4 {
						uuid=+6YGVDtwpVypVzYJj6AAAAC4; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-20000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=inv_tx
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=16000; y1=0; x2=16000; y2=-24000; }
							ha:line { x1=16000; y1=-24000; x2=0; y2=-24000; }
							ha:line { x1=0; y1=-24000; x2=0; y2=0; }
							ha:line { x1=0; y1=0; x2=16000; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.6 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.7 { x1=2000; y1=-4000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=serial
					name=S1
					role=symbol
				}
			}
			ha:group.4 {
				uuid=+6YGVDtwpVypVzYJj6AAAAC+; src_uuid=Q8cSXpoK0QkYCcKy+asAAABC;
				x=58000; y=108000;
				li:objects {
					ha:polygon.5 {
						li:outline {
							ha:line { x1=14000; y1=0; x2=14000; y2=-24000; }
							ha:line { x1=14000; y1=-24000; x2=-2000; y2=-24000; }
							ha:line { x1=-2000; y1=-24000; x2=-2000; y2=0; }
							ha:line { x1=-2000; y1=0; x2=14000; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.6 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.7 { x1=2000; y1=-4000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=button_mx
					name=S2
					role=symbol
				}
			}
			ha:group.5 {
				uuid=+6YGVDtwpVypVzYJj6AAAADI; src_uuid=Q8cSXpoK0QkYCcKy+asAAABC;
				x=56000; y=76000;
				li:objects {
					ha:group.4 {
						uuid=+6YGVDtwpVypVzYJj6AAAADM; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=-4000; y=-12000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vin
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=16000; y1=0; x2=16000; y2=-24000; }
							ha:line { x1=16000; y1=-24000; x2=0; y2=-24000; }
							ha:line { x1=0; y1=-24000; x2=0; y2=0; }
							ha:line { x1=0; y1=0; x2=16000; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.6 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.7 { x1=2000; y1=-4000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=psu
					name=S3
					role=symbol
				}
			}
			ha:group.6 {
				uuid=+6YGVDtwpVypVzYJj6AAAADb; src_uuid=+6YGVDtwpVypVzYJj6AAAADU;
				x=144000; y=96000;
				li:objects {
					ha:text.1 { x1=0; y1=22000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=+6YGVDtwpVypVzYJj6AAAADc; src_uuid=+6YGVDtwpVypVzYJj6AAAADV;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=+6YGVDtwpVypVzYJj6AAAADd; src_uuid=+6YGVDtwpVypVzYJj6AAAADW;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=+6YGVDtwpVypVzYJj6AAAADe; src_uuid=+6YGVDtwpVypVzYJj6AAAADX;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:group.5 {
						uuid=+6YGVDtwpVypVzYJj6AAAADf; src_uuid=+6YGVDtwpVypVzYJj6AAAADY;
						x=0; y=12000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=4
							role=terminal
						}
					}
					ha:group.6 {
						uuid=+6YGVDtwpVypVzYJj6AAAADg; src_uuid=+6YGVDtwpVypVzYJj6AAAADZ;
						x=0; y=16000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=5
							role=terminal
						}
					}
					ha:group.7 {
						uuid=+6YGVDtwpVypVzYJj6AAAADh; src_uuid=+6YGVDtwpVypVzYJj6AAAADa;
						x=0; y=20000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=6
							role=terminal
						}
					}
					ha:polygon.8 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=22000; }
							ha:line { x1=0; y1=22000; x2=4000; y2=22000; }
							ha:line { x1=4000; y1=22000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(6,1)
					name=CN1
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.7 {
				uuid=+6YGVDtwpVypVzYJj6AAAADi;
				li:objects {
					ha:line.1 { x1=76000; y1=140000; x2=128000; y2=140000; stroke=wire; }
					ha:line.2 { x1=128000; y1=140000; x2=128000; y2=116000; stroke=wire; }
					ha:line.3 { x1=128000; y1=116000; x2=140000; y2=116000; stroke=wire; }
					ha:text.4 { x1=108000; y1=140000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=terminal_rx
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.8 {
				li:conn {
					/2/7/1
					/2/3/1/1
				}
			}
			ha:connection.9 {
				li:conn {
					/2/7/3
					/2/6/7/1
				}
			}
			ha:group.10 {
				uuid=+6YGVDtwpVypVzYJj6AAAADj;
				li:objects {
					ha:line.1 { x1=76000; y1=136000; x2=124000; y2=136000; stroke=wire; }
					ha:line.2 { x1=124000; y1=136000; x2=124000; y2=112000; stroke=wire; }
					ha:line.3 { x1=124000; y1=112000; x2=140000; y2=112000; stroke=wire; }
					ha:text.4 { x1=108000; y1=136000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=terminal_tx
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/10/1
					/2/3/2/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/10/3
					/2/6/6/1
				}
			}
			ha:group.13 {
				uuid=+6YGVDtwpVypVzYJj6AAAADk;
				li:objects {
					ha:line.1 { x1=76000; y1=128000; x2=116000; y2=128000; stroke=wire; }
					ha:line.2 { x1=116000; y1=128000; x2=116000; y2=108000; stroke=wire; }
					ha:line.3 { x1=116000; y1=108000; x2=140000; y2=108000; stroke=wire; }
					ha:text.4 { x1=96000; y1=128000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=readout_rx
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.14 {
				li:conn {
					/2/13/1
					/2/3/3/1
				}
			}
			ha:connection.15 {
				li:conn {
					/2/13/3
					/2/6/5/1
				}
			}
			ha:group.16 {
				uuid=+6YGVDtwpVypVzYJj6AAAADl;
				li:objects {
					ha:line.1 { x1=76000; y1=124000; x2=112000; y2=124000; stroke=wire; }
					ha:line.2 { x1=112000; y1=124000; x2=112000; y2=104000; stroke=wire; }
					ha:line.3 { x1=112000; y1=104000; x2=140000; y2=104000; stroke=wire; }
					ha:text.4 { x1=96000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=readout_tx
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.17 {
				li:conn {
					/2/16/1
					/2/3/4/1
				}
			}
			ha:connection.18 {
				li:conn {
					/2/16/3
					/2/6/4/1
				}
			}
			ha:group.19 {
				uuid=+6YGVDtwpVypVzYJj6AAAADm;
				li:objects {
					ha:line.1 { x1=52000; y1=64000; x2=44000; y2=64000; stroke=wire; }
					ha:line.2 { x1=44000; y1=64000; x2=44000; y2=44000; stroke=wire; }
					ha:line.3 { x1=44000; y1=44000; x2=112000; y2=44000; stroke=wire; }
					ha:line.4 { x1=112000; y1=44000; x2=112000; y2=100000; stroke=wire; }
					ha:line.5 { x1=112000; y1=100000; x2=140000; y2=100000; stroke=wire; }
					ha:text.6 { x1=112000; y1=96000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=V12_dc
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.20 {
				li:conn {
					/2/19/1
					/2/5/4/1
				}
			}
			ha:connection.21 {
				li:conn {
					/2/19/5
					/2/6/3/1
				}
			}
			ha:group.22 {
				uuid=+6YGVDtwpVypVzYJj6AAAADn;
				li:objects {
					ha:line.1 { x1=140000; y1=96000; x2=136000; y2=96000; stroke=wire; }
					ha:line.2 { x1=136000; y1=96000; x2=136000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.23 {
				li:conn {
					/2/22/1
					/2/6/2/1
				}
			}
			ha:group.24 {
				uuid=+6YGVDtwpVypVzYJj6AAAADs; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=136000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=+6YGVDtwpVypVzYJj6AAAADt; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1/1
					/2/22/2
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1/4
			print_page=A/4
			title=CPU board, block diagram
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
