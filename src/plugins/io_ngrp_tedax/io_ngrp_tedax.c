/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - tEDAx non-graphical sheet support
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <libcschem/non_graphical.h>
#include <librnd/core/plugins.h>
#include "read.h"

static csch_plug_io_t tdxv1;

static int io_ngrp_tedax_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "tedax") && !strstr(fmt, "tEDAx") && !strstr(fmt, "tdx"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	return 0;
}

static int io_ngrp_tedax_save_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "tedax") && !strstr(fmt, "tEDAx") && !strstr(fmt, "tdx"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	return 0;
}


int pplg_check_ver_io_ngrp_tedax(int ver_needed) { return 0; }

void pplg_uninit_io_ngrp_tedax(void)
{
	csch_non_graphical_impl_pre_unload(&io_ngrp_tedax_impl);
	csch_plug_io_unregister(&tdxv1);
}

int pplg_init_io_ngrp_tedax(void)
{
	RND_API_CHK_VER;

	tdxv1.name = "tEDAx non-graphical schematic sheet v1";
	tdxv1.load_prio = io_ngrp_tedax_load_prio;
	tdxv1.save_prio = io_ngrp_tedax_save_prio;
	tdxv1.load_sheet = io_ngrp_tedax_load_sheet;
	tdxv1.test_parse = io_ngrp_tedax_test_parse;

	tdxv1.ext_save_sheet = "tdx";
	csch_plug_io_register(&tdxv1);
	return 0;
}

