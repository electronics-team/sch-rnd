/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - non-graphical sheet helpers
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Maintain a cache of parsed comp/net/conn objects and compile abstract
   model from them */

#include <libcschem/concrete.h>
#include <genvector/gds_char.h>
#include <genvector/vtp0.h>

typedef struct sch_ngrp_attrib_s sch_ngrp_attrib_t;
typedef struct sch_ngrp_obj_s sch_ngrp_obj_t;
typedef struct sch_ngrp_obj_s sch_ngrp_comp_t;
typedef struct sch_ngrp_obj_s sch_ngrp_net_t;
typedef struct sch_ngrp_conn_s sch_ngrp_conn_t;

struct sch_ngrp_attrib_s {
	char *val;
	long lineno;
	sch_ngrp_attrib_t *next; /* singly linked list */
	char key[1]; /* overallocated to hold both key and val */
};

struct sch_ngrp_obj_s {
	sch_ngrp_attrib_t *attr_head;
	long lineno;
	sch_ngrp_obj_t *next; /* singly linked list */
	csch_ahdr_t *ahdr;    /* used temporarily during sheet compilation */
	char name[1]; /* overallocated */
};

struct sch_ngrp_conn_s {
	char *comp;
	char *port;
	long lineno;
	sch_ngrp_conn_t *next; /* singly linked list */
	char netname[1]; /* overallocated to hold all net, comp and port */
};

typedef struct {
	csch_sheet_t *sheet;
	gds_t text;            /* raw text from the file */
	vtp0_t lines;          /* pointer to the first char of each line */

	/* cache: precompiled */
	sch_ngrp_comp_t *comps; /* head of linked list */
	sch_ngrp_net_t *nets;   /* head of linked list */
	sch_ngrp_conn_t *conns; /* head of linked list */
} sch_ngrp_t;

/* Standard non-graphical sheet callback for compiling a sheet */
int sch_ngrp_compile_sheet(csch_abstract_t *dst, int viewid, csch_hier_path_t *hpath, const csch_sheet_t *src, sch_ngrp_t *ctx);


/* Free all cache fields in ctx, set them to NULL */
void sch_ngrp_clean_cache(sch_ngrp_t *ctx);

/* Free all fields of ctx and ctx itself as well */
void sch_ngrp_free(sch_ngrp_t *ctx);


/* Store different objects in the cache, allocating and linking in a new item;
   if val_len is -1, calls strlen(val). */
sch_ngrp_obj_t *sch_ngrp_add_obj(sch_ngrp_obj_t **head, const char *name, long lineno);
void sch_ngrp_add_attr(sch_ngrp_attrib_t **head, const char *key, const char *val, int val_len, long lineno);
sch_ngrp_conn_t *sch_ngrp_add_conn(sch_ngrp_conn_t **head, const char *netname, const char *comp, const char *port, long lineno);


/* Load a text file into ctx */
int sch_ngrp_load_file(csch_sheet_t *sheet, sch_ngrp_t *ctx, const char *fn);

/* Return text lines for the rendering code */
char *sch_ngrp_draw_getline(sch_ngrp_t *ctx, long *cnt, void **cookie);
