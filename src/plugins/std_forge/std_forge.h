#include <genvector/vts0.h>

/* reverse engineer a standard foormatted test bench conditonal.
   The standard form is: (a.b != "") && (a.b != "foo") && (a.b != "bar") && ...
   Result in dst is triplets of strings, "a", "b" and the literal it should
   be equal to. Returns 0 on success. */
int forge_condition_get_test_bench(vts0_t *dst, const char *objtype, const char *objname, const char *condname, const char *condstr);


