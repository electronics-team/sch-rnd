void csch_dlg_test_bench_init(void);
void csch_dlg_test_bench_uninit(void);

extern const char csch_acts_TestBenchDialog[];
extern const char csch_acth_TestBenchDialog[];
fgw_error_t csch_act_TestBenchDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_quick_attr_forge__if__test_bench[];
extern const char csch_acth_quick_attr_forge__if__test_bench[];
fgw_error_t csch_act_quick_attr_forge__if__test_bench(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_TestBenchModify[];
extern const char csch_acth_TestBenchModify[];
fgw_error_t csch_act_TestBenchModify(fgw_arg_t *res, int argc, fgw_arg_t *argv);
