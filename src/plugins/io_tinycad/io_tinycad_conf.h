#ifndef SCH_RND_IO_TINYCAD_CONF_H
#define SCH_RND_IO_TINYCAD_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_REAL coord_mult;               /* all tinycad coordinates are multiplied by this value to get sch-rnd coords */
			RND_CFT_BOOLEAN emulate_text_ang_180;  /* gschem displays text objects with angle==180 with an extra 180 degree rotation; it's a display hack sch-rnd doesn't have; when this emulation is enabled, the loader adds a +180 degree rotation in such text (changing data!) to match the behavior */
			RND_CFT_BOOLEAN auto_normalize;        /* move all objects so that starting coords are near 0;0 */
			RND_CFT_LIST postproc_sheet_load;      /* pattern;action pairs for object transformations after a succesful load; mostly used for attribute editing */
		} io_tinycad;
	} plugins;
} conf_io_tinycad_t;

extern conf_io_tinycad_t io_tinycad_conf;

#endif
