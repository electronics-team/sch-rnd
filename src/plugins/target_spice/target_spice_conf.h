#ifndef SCH_RND_TARGET_SPICE_CONF_H
#define SCH_RND_TARGET_SPICE_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_STRING bridges;        /* what kind of bdiges to install when a port has spice/bridge/model set; one of: none, vector, scalar */
			RND_CFT_LIST search_paths;     /* ordered list of paths that are each recursively searched for spice module files */
		} target_spice;
	} plugins;
} conf_target_spice_t;

#endif
