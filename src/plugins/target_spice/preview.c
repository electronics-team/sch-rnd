/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - spice target
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

static char *spicelib_mod_preview_text(csch_sheet_t *sheet, csch_lib_t *src, const char *parametric)
{
	const char *mod;
	long oid = src->backend_data.lng[0];

	if (oid == 0) {
		/* external lib */
		csch_hook_call_ctx_t cctx = {0};

		cctx.project = (csch_project_t *)sheet->hidlib.project;
		mod = spicelib_get_extlib(global_spicelib_ctx, src->realpath, &cctx, 0, NULL);
	}
	else {
		/* local lib */
		mod = spicelib_get_from_loclib(sheet, src->name);
	}

	return rnd_strdup(mod == NULL ? "<not found>" : mod);
}

