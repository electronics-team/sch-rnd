#ifndef FIO_H
#define FIO_H

#include <librnd4/librnd/config.h>
#undef fopen

/* similar API to libucdf/ucdf.h */

#include <stdio.h>

typedef struct io_orcad_rctx_s
{
	const char* fn;

	FILE* file;
	int err; /* libucdf have err in ctx, but we don't have ctx */
} io_orcad_rctx_t;

int fio_fopen(io_orcad_rctx_t* const rctx, const char* filename);
long fio_fread(io_orcad_rctx_t* const rctx, char* dst, long len);
int fio_fseek(io_orcad_rctx_t* const rctx, long offs);
int fio_fclose(io_orcad_rctx_t* const rctx); /* not in libucdf */

#endif /* FIO_H */
