#ifndef SCH_RND_RENUMBER_CONF_H
#define SCH_RND_RENUMBER_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_BOOLEAN todo;
		} renumber;
	} plugins;
} conf_renumber_t;

#endif
