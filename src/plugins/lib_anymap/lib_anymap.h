#ifndef SCH_RND_LIB_ANYMAP_H
#define SCH_RND_LIB_ANYMAP_H

#include <load_cache/load_cache.h>
#include <libcschem/plug_library.h>
#include <libcschem/event.h>

typedef struct anymap_ctx_s anymap_ctx_t; /* per view data */
struct anymap_ctx_s {
	const char *name;               /* name of the map (e.g. "devmap", for error messages) */
	const char *attr_key;           /* name of the symbol attribute used to address the map, usually matches ->name */
	const char *eng_src_name;       /* engine name used in attribute sources, e.g. "std_devmap" */
	ldch_ctx_t maps;
	ldch_low_parser_t *low_parser;
	ldch_high_parser_t *high_parser;
	vtp0_t ssyms;                   /* temporary cache between calls to save on mallocs - not persistent */
	csch_lib_backend_t *be;
	void (*sheet_init)(anymap_ctx_t *actx, csch_sheet_t *sheet, csch_lib_t **root_dir_out, int alloc);
}; /* per view data */

typedef struct anymap_obj_s {
	csch_attribs_t comp_attribs;
} anymap_obj_t;


/*** loclib ***/

/* hash: a htsi_t, keyed with indirect devmap groups are stored in the
   indirect/purpose=devmap group's backend_data->ptr[0]; this is used
   for quick lookup of mapobj_name -> grp; key is grp->loclib_name (not
   alloced/free'd for the hash table). backend_data->ptr[0] points
   to the indirect/purpose=devmap grp */
void anymap_sheet_init_(anymap_ctx_t *actx, rnd_design_t *hl, csch_lib_t *root_dir, const csch_cgrp_t *maproot);

int anymap_loc_list(csch_sheet_t *sheet, csch_lib_t *src, const char *attr_key);

int anymap_loc_refresh_from_ext(anymap_ctx_t *actx, csch_sheet_t *sheet, csch_lib_t *src);

csch_attribs_t *anymap_get_from_loclib(anymap_ctx_t *actx, csch_sheet_t *sheet, const char *mapobj_name);

void anymap_lht_free(csch_lib_t *src);


/*** libs ***/

csch_attribs_t *anymap_get_extlib(anymap_ctx_t *ctx, const char *mapobj_name, csch_hook_call_ctx_t *cctx, int save_in_local);

/* Look up and return component attributes for the component's mapobj name:
   - first look in the local lib; if present, load from there
   - if not, look in the library and import into the local lib
   - return NULL if both fail */
csch_attribs_t *anymap_get_from_any_lib(anymap_ctx_t *actx, csch_acomp_t *comp, csch_attrib_t *adm, void *ucallctx);


/*** preview ***/

char *anymap_lht_preview_text(anymap_ctx_t *actx, csch_sheet_t *sheet, csch_lib_t *src, const char *parametric);



#endif
