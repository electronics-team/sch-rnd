
extern const char csch_acts_LibraryDialog[];
extern const char csch_acth_LibraryDialog[];
fgw_error_t csch_act_LibraryDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

/* Called upon CSCH_EVENT_LIBRARY_CHANGED, refreshes active per sheet dialogs */
void csch_dlg_library_changed(csch_sheet_t *sheet);

void csch_dlg_library_preunload(csch_sheet_t *sheet);


void csch_dlg_library_edit(csch_sheet_t *sheet);
void csch_dlg_library_init(void);
void csch_dlg_library_uninit(void);



