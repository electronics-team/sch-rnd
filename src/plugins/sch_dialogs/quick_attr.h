#ifndef SCH_DIALOGS_QUICK_ATTR_H
#define SCH_DIALOGS_QUICK_ATTR_H

#include <libcschem/concrete.h>

/* Returns non-zero if attribute key is quick-editable */
int sch_rnd_attr_quick_editable(csch_sheet_t *sheet, csch_chdr_t *obj, const char *key);

/* Offer GUI for quick-edit an attribute. Returns -1 on error, 0 on no change
   or 1 if the user has changed attribute value(s). */
int sch_rnd_attr_quick_edit(csch_sheet_t *sheet, csch_chdr_t *obj, const char *key);


/*** stock quick edits (core data model) ***/
extern const char csch_acts_quick_attr_role[];
extern const char csch_acth_quick_attr_role[];
fgw_error_t csch_act_quick_attr_role(fgw_arg_t *res, int argc, fgw_arg_t *argv);

/* generic */
extern const char csch_acts_QuickAttr[];
extern const char csch_acth_QuickAttr[];
fgw_error_t csch_act_QuickAttr(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_QuickAttrEditable[];
extern const char csch_acth_QuickAttrEditable[];
#define csch_act_QuickAttrEditable csch_act_QuickAttr

/*** plugin administration ***/
void sch_rnd_attr_quick_uninit(void);


#endif
