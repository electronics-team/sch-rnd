extern const char csch_acts_UndoDialog[];
extern const char csch_acth_UndoDialog[];
fgw_error_t csch_act_UndoDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void csch_dlg_undo_brd_changed_ev(csch_sheet_t *sheet);

void csch_dlg_undo_init(void);
void csch_dlg_undo_uninit(void);
