/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - alien file format helpers
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef SCH_RND_LIB_ALIEN_READ_POSTPROC_H
#define SCH_RND_LIB_ALIEN_READ_POSTPROC_H

/* Standard alien file format postprocessing. Returns 0 on success */
int csch_alien_postproc_sheet(csch_alien_read_ctx_t *ctx);

/* Rebase origin: move sheet's bbox x1;y1 to 0;0 */
void csch_alien_postproc_normalize(csch_alien_read_ctx_t *ctx);

/* recursively emulate tune text rotation; 0 and 90 degree text is always
   kept; 180 or 270 degree text is adjusted depending on the arguments */
void csch_alien_postproc_text_autorot(csch_alien_read_ctx_t *ctx, csch_cgrp_t *grp, int fix180, int fix270);

/* Search for terminals that have the same name within groups (but not group
   refs) and rename them to unique name */
void csch_alien_postproc_rename_redundant_terms(csch_alien_read_ctx_t *ctx);

#endif
