#include <librnd/core/conf.h>
#include <libcschem/abstract.h>
#include <genht/htss.h>

/* Look at each attribute key in the varag list in obj's attribs; the first
   that is found is copied as dst_name in the same obj, using prio and src_desc
   for the source. Works on string attributes only. Returns the source
   attribute key used or NULL if not copied. */
const char *sch_trgt_copy_alt_attrib(csch_ahdr_t *obj, int prio, const char *src_desc, const char *dst_name, ...);


typedef struct sch_trgt_ctx_s {
	htss_t white, black;
	unsigned has_white:1;
	unsigned has_black:1;
	const char *flow_prefix, *sw_prefix;
	int flow_prefix_len, sw_prefix_len;
} sch_trgt_ctx_t;

void sch_trgt_ctx_init(sch_trgt_ctx_t *ctx, const rnd_conflist_t *white, const rnd_conflist_t *black, const char *flow_prefix, const char *sw_prefix);
void sch_trgt_export_attrs(sch_trgt_ctx_t *ctx, csch_ahdr_t *obj);
void sch_trgt_ctx_uninit(sch_trgt_ctx_t *ctx);

