/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - netlist export helpers
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/* Maintain a cache of parsed comp/net/conn objects and compile abstract
   model from them */

#include <libcschem/config.h>

#include <stdio.h>
#include <stdarg.h>
#include <librnd/core/plugins.h>

#include "lib_netlist_exp.h"

const char *sch_nle_get_refdes(const csch_acomp_t *comp)
{
	const char *refdes, *aname;

	refdes = csch_attrib_get_str(&comp->hdr.attr, "display/name");
	if (refdes != NULL)
		return refdes;

	refdes = csch_attrib_get_str(&comp->hdr.attr, "display/refdes");
	if (refdes != NULL)
		return refdes;

	aname = csch_attrib_get_str(&comp->hdr.attr, "name");
	if (aname == NULL) {
		/* Either refdes or no name means do not export; this is to avoid
		   returning comp->name in case the user did not want this component
		   on the output */
		if (refdes == NULL) refdes = csch_attrib_get_str(&comp->hdr.attr, "refdes");
		return refdes;
	}

	if (comp->hdepth == 0) {
		if (refdes == NULL) refdes = aname;
		if (refdes == NULL) refdes = csch_attrib_get_str(&comp->hdr.attr, "refdes");
	}
	else
		refdes = comp->name;

	return refdes;
}

const char *sch_nle_get_pinnum(const csch_aport_t *port)
{
	const char *pinnum;
	pinnum = csch_attrib_get_str(&port->hdr.attr, "display/name");
	if (pinnum == NULL) pinnum = csch_attrib_get_str(&port->hdr.attr, "display/pinnum");
	if (pinnum == NULL) pinnum = csch_attrib_get_str(&port->hdr.attr, "name");
	if (pinnum == NULL) pinnum = csch_attrib_get_str(&port->hdr.attr, "pinnum");
	if (pinnum == NULL) pinnum = port->name;
	return pinnum;
}

const char *sch_nle_get_netname(const csch_anet_t *net)
{
	const char *netname;
	netname = csch_attrib_get_str(&net->hdr.attr, "display/name");
	if ((netname == NULL) && (net->hdepth == 0)) netname = csch_attrib_get_str(&net->hdr.attr, "name");
	if (netname == NULL) netname = net->name;
	return netname;
}


const char *sch_nle_get_alt_attr(const csch_attribs_t *attribs, ...)
{
	const char *val = NULL;
	va_list ap;

	va_start(ap, attribs);
	for(;;) {
		const char *key = va_arg(ap, const char *);
		if (key == NULL)
			break;
		val = csch_attrib_get_str(attribs, key);
		if (val != NULL)
			break;
	}
	va_end(ap);
	return val;
}



/*** plugin ***/

int pplg_check_ver_lib_netlist_exp(int ver_needed) { return 0; }

void pplg_uninit_lib_netlist_exp(void)
{
}

int pplg_init_lib_netlist_exp(void)
{
	RND_API_CHK_VER;

	return 0;
}

