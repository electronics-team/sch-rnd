#include <genvector/vts0.h>


/* Read the next line into dst (overwriting the string there);
   read_cb() is caller provided low level reader, should return
   0 on success (all bytes read). */
int binlen2txt_readline(vts0_t *dst, int (*read_cb)(void *ctx, void *dst, long len), void *ctx);


