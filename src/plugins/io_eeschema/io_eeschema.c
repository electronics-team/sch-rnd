/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - eeschema format support
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <librnd/core/plugins.h>
#include <librnd/core/conf_multi.h>
#include "read.h"

#include "io_eeschema_conf.h"
#include "conf_internal.c"

conf_io_eeschema_t io_eeschema_conf;

static csch_plug_io_t eeschema;
static char eeschema_cookie[] = "io_eeschema";

static int io_eeschema_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "eeschema") && !strstr(fmt, "eeschema") && !strstr(fmt, "sch"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 90;
	return 0;
}

int pplg_check_ver_io_eeschema(int ver_needed) { return 0; }

void pplg_uninit_io_eeschema(void)
{
	csch_plug_io_unregister(&eeschema);
	rnd_conf_plug_unreg("plugins/io_eeschema/", io_eeschema_conf_internal, eeschema_cookie);
}

int pplg_init_io_eeschema(void)
{
	RND_API_CHK_VER;

	eeschema.name = "eeschema schematics sheet v2 or symbol v1";
	eeschema.load_prio = io_eeschema_load_prio;
	eeschema.load_sheet = io_eeschema_load_sheet;
	eeschema.load_grp = io_eeschema_load_grp;
	eeschema.test_parse = io_eeschema_test_parse;

	eeschema.ext_save_sheet = "sch";
	eeschema.ext_save_grp = "sym";
	csch_plug_io_register(&eeschema);


	rnd_conf_plug_reg(io_eeschema_conf, io_eeschema_conf_internal, eeschema_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(io_eeschema_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "io_eeschema_conf_fields.h"

	return 0;
}

