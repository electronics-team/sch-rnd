#include <libcschem/concrete.h>

/* save all sheets */
void sch_rnd_emergency_save(void);

void sch_rnd_disable_emergency_save(void);



/* Saves the board in a backup file using the name configured in conf_core.rc.backup_name */
void sch_rnd_backup(csch_sheet_t *sheet);
void sch_rnd_backup_all(void);

/* Start timer for autosave */
void sch_rnd_enable_autosave(void);


/*** low level/internal ***/
void sch_rnd_save_in_tmp(csch_sheet_t *sheet);
void sch_rnd_save_all_in_tmp(void);
