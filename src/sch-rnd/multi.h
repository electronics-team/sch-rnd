#include <genht/htsp.h>

/* Make s the current sheet (in the GUI). Returns previous current sheet. */
csch_sheet_t *sch_rnd_multi_switch_to(csch_sheet_t *s);

/* Change current sheet to curr+step in the linear sheet list;
   if curr is NULL, use the GUI-current sheet */
void sch_rnd_multi_switch_to_delta(csch_sheet_t *curr, int step);

/* Special case: first switch after loading all pages from the command line */
void sch_rnd_multi_switch_to_initial(csch_sheet_t *s);

csch_project_t *sch_rnd_multi_load_project(const char *load_fn, const char *project_fn_in);


csch_sheet_t *sch_rnd_multi_load(const char *fn, const char *fmt, int *is_project);
csch_sheet_t *sch_rnd_multi_new_empty(const char *fn);
csch_sheet_t *sch_rnd_multi_new_empty_in_prj(csch_project_t *prj, const char *path);

/* App-side loader for external hierarchic child sheets from hlibrary */
csch_sheet_t *sch_rnd_app_load_sheet(csch_project_t *proj, const char *path);

/* Assuming s is not the current sheet, unload it, removing from the list of
   sheets. If s is NULL, unload currently active sheet. */
void sch_rnd_multi_unload(csch_sheet_t *s);

/* Returns whether fn is a path to a project file (project.lht) */
int sch_rnd_is_fn_project_file(const char *fn);


/* Free all sheets currently loaded (useful on exit) */
void sch_rnd_sheet_free_all(void);

void sch_rnd_multi_init(void);
void sch_rnd_multi_uninit(void);
