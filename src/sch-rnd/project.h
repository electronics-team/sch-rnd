void sch_rnd_prj_postproc(csch_project_t *prj);

/* Checks the project file for the given sheet. If it exists, returns 0.
   Else asks the user and creates the project file for sheet; returns 0
   if created, -1 if not (cancel or error; errors are reported in
   the message log) */
int sch_rnd_project_create_file_for_sheet_gui(csch_sheet_t *sheet);

/* Create project file by project file name; update the config of
   all sheets already open for the project and existing dummy project, if any */
int sch_rnd_project_create_file(rnd_design_t *dsg, const char *pname, csch_project_t **prj_out);


/* Create a new empty view and append it in the current project config;
   creates project file if needed */
int sch_rnd_project_append_view(csch_sheet_t *sheet, const char *view_name, int silent);

/* Remove the idxth view from the current project config;
   creates project file if needed */
int sch_rnd_project_del_view(csch_sheet_t *sheet, long idx, int silent);

/* Merge the engine list and save the project file */
void sch_rnd_project_views_save(csch_sheet_t *sheet);

/* Convert view data (engines) into project role config in the backing lihata
   doc of the currently active config tree */
int sch_rnd_project_view_lib2lht(csch_view_t *view);

/* Remove sheet from its current project; remove project if it became empty.
   Return number of sheet removals (should be 0 or 1 normally). */
int sch_rnd_prj_remove_sheet(csch_sheet_t *sheet);
