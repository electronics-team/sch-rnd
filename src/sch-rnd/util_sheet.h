#include <libcschem/concrete.h>

typedef enum sch_rnd_sheet_setup_cfg_e { /* bitfield */
	SCH_RND_SSC_PENS = 1,              /* create pens */
	SCH_RND_SSC_UUID = 2,              /* set up a new uuid for direct */
	SCH_RND_SSC_PEN_MARK_DEFAULT = 4   /* mark new pens copied from default */
} sch_rnd_sheet_setup_cfg_t;

/* Copy defaults into dst (from the default sheet) as cfg dictates; if
   CSCH_SSC_PENS is true chk_copy_pen is called on each pen and the pen
   is copied only if it returns non-zero. If chk_copy_pen is NULL, it is
   assumed to always return 1. */
void sch_rnd_sheet_setup(csch_sheet_t *dst, sch_rnd_sheet_setup_cfg_t cfg, int (*chk_copy_pen)(void *udata, csch_sheet_t *dst, csch_cpen_t *p), void *udata);

/* Change DESIGN role configuration on current sheet for symbol-as-sheet
   editing */
void sch_rnd_reconf_for_symbol_as_sheet(void);

