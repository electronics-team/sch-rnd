#include <libcschem/libcschem.h>

/* On the DAD side the value is stored in .crd in nanometer */
#define RND_DAD_CSCH_COORD(table)  \
do { \
	RND_DAD_SPIN_ANY(table, RND_DAD_SPIN_UNIT_CRD, RND_HATT_COORD, 1, csch_unit_family_cschem); \
	RND_DAD_SPIN_SET_EMPTY_UNIT(table, csch_unit_base); \
	RND_DAD_SPIN_SET_FMT(table, "%$rr"); \
} while(0)

