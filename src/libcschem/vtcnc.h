#ifndef VTCSCH_CNC_H
#define VTCSCH_CNC_H

#include <stdlib.h>
#include <string.h>
#include "libcschem/common_types.h"

#define GVT(x) csch_vtcnc_ ## x

#define GVT_ELEM_TYPE csch_chdr_t*
#define GVT_SIZE_TYPE long
#define GVT_DOUBLING_THRS 4096
#define GVT_START_SIZE 32
#define GVT_FUNC
#define GVT_SET_NEW_BYTES_TO 0
#include <genvector/genvector_impl.h>
#define GVT_REALLOC(vect, ptr, size)  realloc(ptr, size)
#define GVT_FREE(vect, ptr)           free(ptr)
#include <genvector/genvector_undef.h>
#endif
