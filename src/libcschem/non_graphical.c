/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include "concrete.h"
#include "non_graphical.h"

void csch_non_graphical_impl_pre_unload(csch_non_graphical_impl_t *impl)
{
	rnd_design_t *curr;
	for(curr = gdl_first(&rnd_designs); curr != NULL; curr = gdl_next(&rnd_designs, curr)) {
		csch_sheet_t *sheet = (csch_sheet_t *)curr;
		if (sheet->non_graphical && (sheet->non_graphical_impl == impl) && (impl->free_sheet != NULL)) {
			impl->free_sheet(sheet);
			sheet->non_graphical_impl = NULL;
			sheet->non_graphical_data = NULL;
		}
	}
}
