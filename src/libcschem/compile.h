/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_COMPILE_H
#define CSCH_COMPILE_H

#include <libcschem/abstract.h>
#include <libcschem/concrete.h>
#include <libcschem/project.h>

int csch_compile_sheet(csch_abstract_t *dst, int viewid, const csch_sheet_t *src);
int csch_compile_post(csch_project_t *proj, int viewid, csch_abstract_t *dst);

int csch_compile_project(csch_project_t *prj, int viewid, csch_abstract_t *dst, int quiet);


/*** utility for plugins ***/

/* Compile attribute src of cgrp into dst. If dstkey is NULL, srca's
   key is used. src is used for history and is always freed.
   Err1, err2 and err3 are three strings printed in error messages
   for location, with space in between. If referee is 1, src is a group
   ref. If append is 1, append src array to dst array (fails if either
   src or dst attrib is not an array). If append is 0, but src key starts
   with '+', append is set to 1 internally. If dsta_out is not NULL,
   destination attribute (abstract model) is returned there.
   Returns 0 on success. */
int csch_compile_attribute(csch_ahdr_t *dst, const char *dstkey, const csch_attrib_t *srca, csch_source_arg_t *src, const char *err1, const char *err2, const char *err3, int referee, int append, csch_attrib_t **dsta_out);


/* For compiler hooks in plugins */
int csch_compile_connect_net_to(csch_anet_t **net, csch_ahdr_t *a, int allow_reconn);
int csch_compile_disconnect(csch_ahdr_t *a);

/* Updates hdr cache fields, e.g. dnp and omit, from attributes; useful after
   setting such an attribute or before depending on such an attribute */
void csch_compile_update_obj_cache(csch_ahdr_t *obj);


/* Descend in hierarchy to compile the child sheet for dst; srs is the sheet
   ref symbol, name_loc is the short name of the srs that is going to be
   used as the next segment in the hierarchic path. Returns 0 on success. */
int csch_compile_descend(csch_abstract_t *dst, int viewid, csch_hier_path_t *hpath, csch_cgrp_t *srs, char *name_loc, csch_sheet_t *child);


/*** for lib itnernal use ***/

RND_INLINE void csch_compile_add_source(csch_cgrp_t *src, csch_ahdr_t *abst)
{
	vtl0_append(&src->aid, abst->aid);
	vtp0_append(&abst->srcs, src);
}

int csch_compile_sheet_(csch_abstract_t *dst, int viewid, csch_hier_path_t *hpath, const csch_sheet_t *src);

#endif
