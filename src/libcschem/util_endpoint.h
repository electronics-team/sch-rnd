#ifndef LIBCSCHEM_UTIL_ENDPOINT_GRP
#define LIBCSCHEM_UTIL_ENDPOINT_GRP

#include <libcschem/concrete.h>
#include <libcschem/htepu.h>

/* List endpoint connected wirenet objects for obj from the same wirenet
   in dst. If incl is not NULL, it is called on each candidate object before
   collecting potential intersection points; if incl returns zero, the object
   is not included/considered. */
void csch_endpoint_list_connected(csch_sheet_t *sheet, htepu_t *dst, csch_chdr_t *obj, int (*incl)(void *udata, csch_chdr_t *obj), void *udata);

#endif
