/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "concrete.h"

/* Do not run automatic integrity checks if 0 */
extern int csch_integrity_auto;

int csch_integrity_project(csch_project_t *proj, const char *called_from);
int csch_integrity_sheet(csch_sheet_t *sheet, const char *called_from);


#define CSCH_IGY_LOC__(func, line) func ":" #line
#define CSCH_IGY_LOC_(func, line) CSCH_IGY_LOC__(func, line)
#define CSCH_IGY_LOC CSCH_IGY_LOC_(__FILE__, __LINE__)
#define CSCH_IGY_AUTO(igy)  (csch_integrity_auto ? (igy) : 0)

#define CSCH_INTEGRITY_SHEET(sheet) csch_integrity_sheet(sheet, CSCH_IGY_LOC)
#define CSCH_INTEGRITY_SHEET_AUTO(sheet) CSCH_IGY_AUTO(CSCH_INTEGRITY_SHEET(sheet))


void csch_integrity_act_init(void);
void csch_integrity_act_uninit(void);
