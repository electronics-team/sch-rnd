/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include GENGEO2D_TYPECFG
#include "gengeo2d/prim.h"

/* Returns 1 if P is within triangle A-B-C */
int csch_point_in_triangle(g2d_vect_t A, g2d_vect_t B, g2d_vect_t C, g2d_vect_t P);

/* Returns 1 if any point of P (of length npts) is within triangle A-B-C;
   This is more efficient than multiple calls to  csch_point_in_triangle()
   with the same triangle, because some claculations are done only once. */
int csch_any_point_in_triangle(g2d_vect_t A, g2d_vect_t B, g2d_vect_t C, g2d_vect_t *P, int npts);
