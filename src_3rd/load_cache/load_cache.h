/*
    load_cache - cache loading/parsing files
    Copyright (C) 2020  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Project page:  http://repo.hu/projects/libminuid
    Source code:   svn://repo.hu/libminuid/trunk
    Author:        emailto: libminuid [at] igor2.repo.hu
*/

#ifndef LOAD_CACHE_H
#define LOAD_CACHE_H

#include <genht/htsp.h>
#include <genvector/vtp0.h>

typedef struct ldch_ctx_s ldch_ctx_t;
typedef struct ldch_data_s ldch_data_t;
typedef struct ldch_file_s ldch_file_t;
typedef struct ldch_low_parser_s ldch_low_parser_t;
typedef struct ldch_high_parser_s ldch_high_parser_t;
typedef int ldch_uid_t;

struct ldch_low_parser_s {
	char *name;
	ldch_uid_t uid;
	ldch_file_t *(*parse_alloc)(ldch_low_parser_t *parser, void *call_ctx, const char *fn); /* called when the file is first loaded; should allocate (ldch_file_t *) */
	int (*parse)(ldch_low_parser_t *parser, void *call_ctx, ldch_file_t *file, const char *fn); /* called after parse_alloc() on initial load or after free_payload() on reload */
	void (*free_payload)(ldch_low_parser_t *low, ldch_file_t *file); /* should free the in-memory document/parse-tree; a call to parse() may follow when reloading */
	void (*unreg)(ldch_low_parser_t *low);
	ldch_ctx_t *ctx;
	void *used_data;
};

struct ldch_high_parser_s {
	char *name;
	ldch_data_t *(*parse)(ldch_high_parser_t *parser, void *call_ctx, ldch_file_t *file);
	void (*free_payload)(ldch_data_t *data);
	void (*unreg)(ldch_high_parser_t *high);
	ldch_ctx_t *ctx;
	void *used_data;
};

struct ldch_data_s {
	ldch_file_t *parent;
	ldch_high_parser_t *high_parser;
	size_t payload_size;
	char payload[1];   /* real size: payload_size (caches parsed data) */
};

struct ldch_file_s {
	ldch_ctx_t *parent;
	char *load_name;      /* file name: as the user refers to it (strdup'd variant, also the hash key) */
	char *real_name;      /* file name: full path on the file system */
	vtp0_t data;          /* -> (ldch_data_t *), indexed by high_parser uid */
	void *user_data;
	ldch_low_parser_t *low_parser;
	double last_low_parsed; /* time stamp the file was last low-level parsed */
	size_t low_payload_size;
	char low_payload[1];  /* real size: low_payload_size (caches low level parsed data) */
};

struct ldch_ctx_s {
	htsp_t low_parsers;
	htsp_t high_parsers;
	htsp_t files;          /* of ldch_file_t */
	char *(*load_name_to_real_name)(ldch_ctx_t *ctx, const char *load_name, ldch_low_parser_t *low, ldch_high_parser_t *high, void *low_call_ctx, void *high_call_ctx); /* simple strdup() if NULL */
	void *user_data;
	ldch_uid_t next_low_uid;
};

/* The host application may keep multiple, independent cache contexts */
void ldch_init(ldch_ctx_t *ctx);
void ldch_uninit(ldch_ctx_t *ctx);

/* Parsers need to be registered in the context so they can be referenced by name.*/
ldch_low_parser_t *ldch_reg_low_parser(ldch_ctx_t *ctx, const char *name);
ldch_high_parser_t *ldch_reg_high_parser(ldch_ctx_t *ctx, const char *name);
ldch_low_parser_t *ldch_lookup_low_parser(ldch_ctx_t *ctx, const char *name);
ldch_high_parser_t *ldch_lookup_high_parser(ldch_ctx_t *ctx, const char *name);

/* Load a file from disk or from memory; in any case if the modification timestamp
   on disk is higher than the cached timestamp, reload the file */
ldch_data_t *ldch_load_(ldch_ctx_t *ctx, const char *load_name, ldch_low_parser_t *low, ldch_high_parser_t *high, void *low_call_ctx, void *high_call_ctx);
ldch_data_t *ldch_load(ldch_ctx_t *ctx, const char *load_name, const char *low, const char *high, void *low_call_ctx, void *high_call_ctx);

/* Remove a single file from a context, freeing all memory taken by the parsed
   data. Will be re-loaded upon next request */
void ldch_unload(ldch_ctx_t *ctx, ldch_file_t *file);


/*** calls for parsers ***/
ldch_data_t *ldch_data_alloc(ldch_file_t *file, ldch_high_parser_t *high_parser, size_t payload_size);
void ldch_data_free(ldch_ctx_t *ctx, ldch_data_t *data);

ldch_file_t *ldch_file_alloc(ldch_ctx_t *ctx, ldch_low_parser_t *low_parser, size_t low_payload_size);

#endif
