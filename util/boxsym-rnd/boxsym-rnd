#!/bin/sh
#                            COPYRIGHT
#
#  cschem - modular/flexible schematics editor - box symbol generator utility
#  Copyright (C) 2022 Tibor 'Igor2' Palinkas
#
#  (Supported by NLnet NGI0 PET Fund in 2022)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#  Contact:
#    Project page: http://repo.hu/projects/sch-rnd
#    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
#    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html

PREFIX="`dirname $0`/.."

if test -z "$1"
then
	echo "Need an input file name" >&2
	exit 1
fi

if test -f util.awk -a -f generator.awk
then
	# running from source
	LIBDIR=.
	COMSYM=../../library/symbol/common_sym.awk
	MINUID=../../src_3rd/libminuid/minuid
else
	# running the installed version
	LIBDIR=$PREFIX/lib/sch-rnd/boxsym-rnd
	COMSYM=$PREFIX/share/sch-rnd/symbol/common_sym.awk
	MINUID=$PREFIX/lib/sch-rnd/minuid
fi

awk -v "MINUID=$MINUID" -f "$LIBDIR/util.awk" -f "$LIBDIR/parser.awk" \
    -f "$COMSYM" -f "$LIBDIR/generator.awk" "$1"
