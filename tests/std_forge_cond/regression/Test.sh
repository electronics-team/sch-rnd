#!/bin/sh
tester=../std_forge_cond
bad=0
for n in *.in
do
	base=${n%%.in}
	$tester <$n >$base.out 2>&1
	diff $base.ref $base.out
	if test "$?" = "0"
	then
		rm $base.out
	else
		bad=1
	fi
done

if test "$bad" = "0"
then
	echo "*** QC PASS ***"
fi
