ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=JKMrlvmRpJSOuz679xUAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.8 {
				uuid=JKMrlvmRpJSOuz679xUAAAAG;
				x=112000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=JKMrlvmRpJSOuz679xUAAAAH; src_uuid=JKMrlvmRpJSOuz679xUAAAAD;
						x=4000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in
							role=terminal
						}
					}
					ha:group.2 {
						uuid=JKMrlvmRpJSOuz679xUAAAAI; src_uuid=JKMrlvmRpJSOuz679xUAAAAD;
						x=8000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
						}
					}
					ha:polygon.3 {
						li:outline {
							ha:line { x1=4000; y1=4000; x2=4000; y2=-4000; }
							ha:line { x1=4000; y1=-4000; x2=8000; y2=0; }
							ha:line { x1=8000; y1=0; x2=4000; y2=4000; }
						}
						stroke=sym-decor;
					}
					ha:text.4 { x1=3000; y1=4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=child.rs
					name=S1
					role=symbol
				}
			}
			ha:group.9 {
				uuid=JKMrlvmRpJSOuz679xUAAAAP; src_uuid=JKMrlvmRpJSOuz679xUAAAAG;
				x=132000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=JKMrlvmRpJSOuz679xUAAAAQ; src_uuid=JKMrlvmRpJSOuz679xUAAAAD;
						x=4000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in
							role=terminal
						}
					}
					ha:group.2 {
						uuid=JKMrlvmRpJSOuz679xUAAAAR; src_uuid=JKMrlvmRpJSOuz679xUAAAAD;
						x=8000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
						}
					}
					ha:polygon.3 {
						li:outline {
							ha:line { x1=4000; y1=4000; x2=4000; y2=-4000; }
							ha:line { x1=4000; y1=-4000; x2=8000; y2=0; }
							ha:line { x1=8000; y1=0; x2=4000; y2=4000; }
						}
						stroke=sym-decor;
					}
					ha:text.4 { x1=3000; y1=4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=child.rs
					name=S2
					role=symbol
				}
			}
			ha:group.10 {
				uuid=JKMrlvmRpJSOuz679xUAAAAS;
				li:objects {
					ha:line.1 { x1=124000; y1=84000; x2=132000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/10/1
					/2/8/2/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/10/1
					/2/9/1/1
				}
			}
			ha:group.13 {
				uuid=JKMrlvmRpJSOuz679xUAAAAT;
				li:objects {
					ha:line.1 { x1=112000; y1=84000; x2=104000; y2=84000; stroke=wire; }
					ha:text.2 { x1=107000; y1=84000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=inp
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.14 {
				li:conn {
					/2/13/1
					/2/8/1/1
				}
			}
			ha:group.15 {
				uuid=JKMrlvmRpJSOuz679xUAAAAY; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=80000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=JKMrlvmRpJSOuz679xUAAAAZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.17 {
				uuid=JKMrlvmRpJSOuz679xUAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=152000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=JKMrlvmRpJSOuz679xUAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=JKMrlvmRpJSOuz679xUAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R2
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=10k
				}
			}
			ha:group.18 {
				uuid=JKMrlvmRpJSOuz679xUAAAAj;
				li:objects {
					ha:line.1 { x1=144000; y1=84000; x2=152000; y2=84000; stroke=wire; }
					ha:text.2 { x1=146000; y1=84000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=outp
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.19 {
				li:conn {
					/2/18/1
					/2/9/2/1
				}
			}
			ha:connection.20 {
				li:conn {
					/2/18/1
					/2/17/2/1
				}
			}
			ha:group.21 {
				uuid=JKMrlvmRpJSOuz679xUAAAAk;
				li:objects {
					ha:line.1 { x1=172000; y1=84000; x2=172000; y2=76000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.22 {
				li:conn {
					/2/21/1
					/2/17/1/1
				}
			}
			ha:group.23 {
				uuid=JKMrlvmRpJSOuz679xUAAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=172000; y=76000;
				li:objects {
					ha:group.1 {
						uuid=JKMrlvmRpJSOuz679xUAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.24 {
				li:conn {
					/2/23/1/1
					/2/21/1
				}
			}
			ha:group.25 {
				uuid=BI86VBN8v61qXqHhRMoAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=84000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=BI86VBN8v61qXqHhRMoAAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=BI86VBN8v61qXqHhRMoAAAAs; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R1
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=10k
				}
			}
			ha:connection.26 {
				li:conn {
					/2/25/1/1
					/2/13/1
				}
			}
			ha:group.28 {
				uuid=BI86VBN8v61qXqHhRMoAAAAt;
				li:objects {
					ha:line.1 { x1=80000; y1=84000; x2=84000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.29 {
				li:conn {
					/2/28/1
					/2/25/2/1
				}
			}
			ha:connection.30 {
				li:conn {
					/2/28/1
					/2/15/1/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
