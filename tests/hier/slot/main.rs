ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=sJumnHXrZ0A1+is6v5YAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.6 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAG;
				x=76000; y=124000;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=-4000; y1=4000; x2=-4000; y2=-4000; }
							ha:line { x1=-4000; y1=-4000; x2=8000; y2=-4000; }
							ha:line { x1=8000; y1=-4000; x2=8000; y2=4000; }
							ha:line { x1=8000; y1=4000; x2=-4000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:group.2 {
						uuid=sJumnHXrZ0A1+is6v5YAAAAH; src_uuid=sJumnHXrZ0A1+is6v5YAAAAD;
						x=12000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=outA
							role=terminal
						}
					}
					ha:group.3 {
						uuid=sJumnHXrZ0A1+is6v5YAAAAI; src_uuid=sJumnHXrZ0A1+is6v5YAAAAD;
						x=-8000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=inA
							role=terminal
						}
					}
					ha:text.4 { x1=-4000; y1=4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.5 { x1=-2000; y1=-2000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=child.rs
					name=S1
					role=symbol
				}
			}
			ha:group.7 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAM; src_uuid=sJumnHXrZ0A1+is6v5YAAAAG;
				x=76000; y=112000;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=-4000; y1=4000; x2=-4000; y2=-4000; }
							ha:line { x1=-4000; y1=-4000; x2=8000; y2=-4000; }
							ha:line { x1=8000; y1=-4000; x2=8000; y2=4000; }
							ha:line { x1=8000; y1=4000; x2=-4000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:group.2 {
						uuid=sJumnHXrZ0A1+is6v5YAAAAN; src_uuid=sJumnHXrZ0A1+is6v5YAAAAD;
						x=12000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=outB
							role=terminal
						}
					}
					ha:group.3 {
						uuid=sJumnHXrZ0A1+is6v5YAAAAO; src_uuid=sJumnHXrZ0A1+is6v5YAAAAD;
						x=-8000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=inB
							role=terminal
						}
					}
					ha:text.4 { x1=-4000; y1=4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.5 { x1=-2000; y1=-2000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=child.rs
					name=S1
					role=symbol
				}
			}
			ha:group.8 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAP;
				li:objects {
					ha:line.1 { x1=68000; y1=124000; x2=62000; y2=124000; stroke=wire; }
					ha:text.2 { x1=58000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=net_ina
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.9 {
				li:conn {
					/2/8/1
					/2/6/3/1
				}
			}
			ha:group.12 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAT; src_uuid=sJumnHXrZ0A1+is6v5YAAAAP;
				x=0; y=-12000;
				li:objects {
					ha:line.1 { x1=68000; y1=124000; x2=62000; y2=124000; stroke=wire; }
					ha:text.2 { x1=58000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=net_inb
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.13 {
				li:conn {
					/2/12/1
					/2/7/3/1
				}
			}
			ha:group.14 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAU;
				li:objects {
					ha:line.1 { x1=88000; y1=124000; x2=98000; y2=124000; stroke=wire; }
					ha:text.2 { x1=90000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=net_outa
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.15 {
				li:conn {
					/2/14/1
					/2/6/2/1
				}
			}
			ha:group.17 {
				uuid=sJumnHXrZ0A1+is6v5YAAAAY; src_uuid=sJumnHXrZ0A1+is6v5YAAAAU;
				x=0; y=-12000;
				li:objects {
					ha:line.1 { x1=88000; y1=124000; x2=98000; y2=124000; stroke=wire; }
					ha:text.2 { x1=90000; y1=124000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=net_outb
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.18 {
				li:conn {
					/2/17/1
					/2/7/2/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
